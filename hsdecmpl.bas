' HSDECMPL, Ralph Versteegen 2003, 2011, 2017
'
' Released into the Public Domain
' (There is absolutely no reason to reuse any of this horrible code though)
'
' Compile with:
' fbc -arch 32 hsdecmpl.bas

'$lang: "deprecated"

'included only for $inclib?
#include once "crt.bi"
'#include "crt/setjmp.bi"
' setjmp.bi is incorrect
type crt_jmp_buf:dummy(63) as byte:end type
#ifdef __FB_WIN32__
declare function setjmp cdecl alias "_setjmp" (byval as any ptr) as integer
#else
declare function setjmp cdecl alias "setjmp" (byval as any ptr) as integer
#endif
declare sub longjmp cdecl alias "longjmp" (byval as any ptr, byval as integer)
#undef rand
#undef abort
#undef bound
#undef strlen

'extern gosubbuf() as crt_jmp_buf
'extern gosubptr as integer
option nokeyword gosub
option nokeyword return
#define gosub if setjmp(@gosubbuf(gosubptr)) then gosubptr-=1 else gosubptr+=1:goto
#define return longjmp(@gosubbuf(gosubptr-1),1)
#define retrievestate gosubptr=localgosubptr
#define rememberstate localgosubptr=gosubptr

dim shared gosubbuf(31) as crt_jmp_buf
dim shared gosubptr as integer = 0


DECLARE SUB timelesswait ()
DECLARE SUB printoutput ()
DECLARE FUNCTION normalstring$ (instring$)
DECLARE SUB readrulefile ()
DECLARE SUB readHS ()
DECLARE SUB decomplallscripts ()
DECLARE SUB includedebug ()
DECLARE SUB readHSdebug ()
DECLARE SUB Matchscripts ()
DECLARE SUB readcommandstr ()
DECLARE SUB simplesortglobals ()
DECLARE SUB outputthefile ()
DECLARE SUB matchscriptsdebug ()
DECLARE SUB optionsselection ()
DECLARE SUB writescriptheader (noargs, novars, scriptnumber)
DECLARE SUB writescriptender ()
DECLARE SUB writeformatfile (fname$)
DECLARE SUB readformatfile (fname$)
DECLARE SUB setstyleoption (opno, value)
DECLARE FUNCTION funcname$ (funcid)
DECLARE FUNCTION functname$ (funcid)
DECLARE SUB readallincludes ()
DECLARE SUB readinclude ()
'DECLARE FUNCTION getscript (id)
DECLARE FUNCTION processoperator$ (wordref, id)
DECLARE FUNCTION findoperator (cmdkind, cmdid)
DECLARE FUNCTION normalizestring$ (instring$)   'Fully working
DECLARE FUNCTION returnscriptdata (jobtype, id)
DECLARE FUNCTION styleoption (opno)
DECLARE SUB errordeal OVERLOAD (errorstring$, foundvalue)
DECLARE SUB errordeal OVERLOAD (errorstring$, foundstring$)
DECLARE SUB warndeal (warnstring$)
DECLARE FUNCTION getword (wordref)
DECLARE FUNCTION processcommand$ (wordref, parentrule = 0, argnum = 0)
DECLARE FUNCTION processmath$ (wordref)
DECLARE FUNCTION intvariable$ (wordref)
DECLARE FUNCTION processflow$ (wordref, canENL, attached_to_if = 0)
DECLARE FUNCTION returnlocalname$ (localno)
DECLARE FUNCTION returnglobalname$ (globalno)
DECLARE FUNCTION giveconditionopen$ (nocmds)
DECLARE FUNCTION giveconditionclose$ (nocmds)
DECLARE FUNCTION giveflowopen$ (nocmds)
DECLARE FUNCTION giveflowclose$ (nocmds)
DECLARE SUB debug (dbstr$)
DECLARE SUB processcmd (cmdstring$, foundtype$)
DECLARE SUB splitline (cmdstring$, rawstring$, searchstart)
DECLARE FUNCTION processoldstring$ (cmdid, wordref)
DECLARE FUNCTION processnewstring$ (cmdid, wordref)
DECLARE FUNCTION getconstantfromrule$ (rule, value)
DECLARE SUB addconstanttorule (rule, foundtype$, value)
DECLARE SUB addconstant (cmdstring$, found$, value)

TYPE StrNum
 str as string
 num as integer
END TYPE

TYPE LookupTbl
 num as integer
 size as integer
 table as StrNum ptr
END TYPE

TYPE constant_t
 str as string
 real as string
 value as integer
END TYPE

TYPE ScriptHeader
 codeoffset as integer
 vars as integer
 args as integer
 scrformat as integer
 strtable as integer
END TYPE

'Global variables
DIM SHARED readstate, currentfile, scriptlocals, nooperators, noscripts, nolumps, nofunctions, scripts_txt
DIM SHARED includefile, currentindent, nosamenameops, outputfile, inputindex, pointerblock
DIM SHARED hsfile$, noincludes, includedscripts, highestfunc, nohighfuncs, nofoundglobals
DIM SHARED noaddglobals, tempfile, decmplscripts, hssfile$, starttime, outputcmd$ 'Big global
DIM SHARED newline$, stacksize', nocurrentvars 'not really needed, except for error checking
DIM SHARED setstring, appendstring

DIM SHARED thisscr as ScriptHeader

DIM SHARED hardfunction$()
DIM SHARED operatorname$(), operatordata(), scriptdata()
DIM SHARED scriptname$(), tempopreal$()
DIM SHARED lumppointerindex(), lumpnames$()
DIM SHARED includedscriptid(), includedscriptname$()
DIM SHARED globalsdec()

DIM SHARED constantlookup() as LookupTbl
DIM SHARED constants() as constant_t
DIM SHARED funcrules()

DIM SHARED styleoptions(0 TO 10)'works by bits, negative exculded. 248 options (I think only use 45)
DIM SHARED globalname$(0 TO 1024), mathfuncname$(0 TO 22), builtinop$(0 TO 20)
DIM SHARED highfunction$(0 TO 20), highfunctionid(0 TO 20), includenames$(0 TO 10)
DIM SHARED stylevars(0 TO 9) AS SHORT

DIM SHARED includedscript_matched(1000), hsscript_matched(1000)

newline$ = CHR$(13) + CHR$(10)
'Constants for style options

CONST usebeginendtoplevel = 1, separateglobals = 2, separatescripts = 3 ' topleveldontdoflow just prints clean brackets or begin, end
CONST renumberautonos = 4, indentautonoscripts = 5, linebetweenscripts = 6
CONST writesindorder = 7, writedinsorder = 8, sortscripts = 9 'delete this?

CONST namevarsfrom1 = 10, separatevariables = 11, dontindentvars = 12, nameargsseparate = 13, lineaftervariables = 14


CONST usebeginendflow = 20, flowbeginnewline = 21, flowbeginindented = 22  'the flow begin, end newline, indented used for conditions too.
CONST flowendnewline = 23, flowendindented = 24, newlineafterend = 25
CONST beginendcondition = 26, conditionnewline = 27, conditionsplit = 28

CONST sortglobals = 30, usefoundname = 31, useoperators = 32, usedscriptcons = 33
CONST usesetvariable = 34, missforstep = 35, simplify1arg = 36 'for cmds and conditions
' , normalizefontcapitals = 37

CONST HSIdebugon = 40, HSdebugon = 41, MSdebugon = 42, noformatneeded = 43  'These options turned on by the commandline reading procedure

'Constants for style variables
CONST spacesindent = 0, baseindent = 1, defineindent = 2

'Names of the math functions
mathfuncname$(0) = "random"
mathfuncname$(1) = "exponent"
mathfuncname$(2) = "modulus"
mathfuncname$(3) = "divide"
mathfuncname$(4) = "multiply"
mathfuncname$(5) = "subtract"
mathfuncname$(6) = "add"
mathfuncname$(7) = "xor"
mathfuncname$(8) = "or"
mathfuncname$(9) = "and"
mathfuncname$(10) = "equal"
mathfuncname$(11) = "notequal"
mathfuncname$(12) = "lessthan"
mathfuncname$(13) = "greaterthan"
mathfuncname$(14) = "lessthanorequalto"
mathfuncname$(15) = "greaterthanorequalto"
mathfuncname$(16) = "setvariable"
mathfuncname$(17) = "increment"
mathfuncname$(18) = "decrement"
mathfuncname$(19) = "not"
mathfuncname$(20) = "logand"
mathfuncname$(21) = "logor"
mathfuncname$(22) = "logxor"
'Builtin comma-less commands. Used when reading include files
builtinop$(0) = "("
builtinop$(1) = ")"  'these aren't really builtin operators, they're just auto-comma-ed commands
builtinop$(2) = "+="
builtinop$(3) = "-="
builtinop$(4) = "$+"
builtinop$(5) = "--"
builtinop$(6) = ":="
builtinop$(7) = "*"
builtinop$(8) = "/"
builtinop$(9) = "=="
builtinop$(10) = ">>"
builtinop$(11) = "<<"
builtinop$(12) = "<="
builtinop$(13) = ">="
builtinop$(14) = "^^"
builtinop$(15) = "^"
builtinop$(16) = "<>"
builtinop$(17) = "$="
builtinop$(18) = "+"
builtinop$(19) = "||"
builtinop$(20) = "&&"

'Function/Sub argument constants
CONST arrayno = 0, scriptarg = 1, defaultarg = 2
CONST scriptid = 0, scriptargs = 1 'These are for the scriptdata array, which has 2 dims
CONST nostate = 0, opendfunction = 1, readfunctionid = 2, readfuncname = 3
CONST readfuncargcount = 4, readfuncargs = 5, opendoperator = 6
CONST readoperatorval = 7, readoperatorname = 8, readoperatorreal = 9, commentedout = 10
CONST opendscript = 11, readscriptid = 12, readscriptname = 13, readscriptsnargs = 14
CONST readscriptargs = 15, opendglobal = 16, readglobalid = 17, readglobalname = 18
CONST opendconstant = 19, readconstantid = 20, readconstantname = 21
CONST readscriptblockname = 22, opendtrigger = 23
'For opertordata
CONST operatorval = 0, operatorkind = 1, operatorid = 2, opmustbeused = 3
'Start of code


'IF scriptlocals > 0 THEN
' REDIM localname$(0 TO (scriptlocals - 1))
'END IF
'REDIM includenames$(0)

'CLS


'---------------Call Subs---------------

'splitline "14--=4+=(^^^:=43(aba$=food))", "   14 -- =4+ =( ^ ^^:=43(Ab a$ = Dood )) ", 0
'END



readcommandstr

'INPUT "testcase"; nnm$
'OPEN nnm$ + ".txt" FOR BINARY AS 18
'PRINT "LEN test is "; LOF(18)
'PRINT "cur dir is "; CURDIR$("D")


IF hsfile$ = "" OR hssfile$ = "" OR noincludes = 0 OR styleoption(noformatneeded) = 0 THEN optionsselection

'PRINT "First 31 values of bitset table checksum:"; styleoptions(0)

'stabs TEH heart
PRINT
starttime = TIMER * 1000
readallincludes
IF styleoption(HSIdebugon) THEN includedebug
readrulefile
readHS
IF styleoption(HSdebugon) THEN readHSdebug
Matchscripts
IF styleoption(MSdebugon) THEN matchscriptsdebug
decomplallscripts
IF styleoption(sortglobals) THEN simplesortglobals
outputthefile
printoutput

IF styleoption(0) THEN COLOR 4: PRINT "Uh oh! Decompiler bug - bitset table corrupted"
'PRINT "First 31 values of bitset table checksum:"; styleoptions(0)
'PRINT "Sig 2 is"; (styleoptions(1) MOD 1024)
temp = TIMER * 1000
PRINT
COLOR 15: PRINT "Done in"; ((temp - starttime) / 1000); " seconds."
PRINT "[Press any key]"
timelesswait
CLOSE
'------------------Fin------------------

'(styleoption(conditionnewline) OR styleoption(conditionsplit))

SUB decomplallscripts

PRINT "Decompiling scripts";
REDIM globalsdec(0 TO 19)
tempfile = FREEFILE
OPEN "HSDCMP03.TMP" FOR BINARY AS tempfile
IF LOF(tempfile) > 0 THEN
 CLOSE tempfile
 KILL "HSDCMP03.TMP"
 OPEN "HSDCMP03.TMP" FOR BINARY AS tempfile
END IF


FOR ctry = 0 TO nolumps - 1
 hsxname$ = lumpnames$(ctry)

 IF RIGHT$(hsxname$, 4) = ".HSX" OR RIGHT$(hsxname$, 4) = ".HSZ" THEN
  scriptidno = VAL(LEFT$(hsxname$, INSTR(hsxname$, ".") - 1))
'   PRINT "ID is"; scriptidno;
  hsxsnap = returnscriptdata(arrayno, scriptidno)
'   PRINT "array index is"; hsxsnap
 '  timelesswait
  IF hsscript_matched(hsxsnap) = 0 THEN 'If the script was not included
   debug "Now decompiling script " & scriptname$(hsxsnap)

   pointerblock = lumppointerindex(ctry)   'offset to the script

   'read header
   DIM temp as SHORT

   GET #currentfile, pointerblock, temp
   thisscr.codeoffset = temp
  
   GET #currentfile, , temp
   thisscr.vars = temp

   IF thisscr.codeoffset >= 6 THEN
    GET #currentfile, , temp
    thisscr.args = temp
   ELSE
    thisscr.args = scriptdata(hsxsnap, scriptargs)  '?
   END IF

   IF thisscr.codeoffset >= 8 THEN
    GET #currentfile, , temp
    thisscr.scrformat = temp
   ELSE
    thisscr.scrformat = 0
   END IF

   IF thisscr.codeoffset >= 12 THEN
    GET #currentfile, , thisscr.strtable
    thisscr.strtable = pointerblock + thisscr.strtable 
   ELSEIF thisscr.codeoffset >= 10 THEN
    GET #currentfile, , temp
    thisscr.strtable = pointerblock + temp
   ELSE
    thisscr.strtable = 0
   END IF



   decmplscripts = decmplscripts + 1

 '  PRINT "now for script header"
   writescriptheader(thisscr.args, thisscr.vars, hsxsnap)

   IF getword(0) <> 2 OR getword(1) <> 0 THEN
    errordeal("Could not find do() at start of script. Found", getword(0) & " " & getword(1))
   END IF

   'decompile each script cmd by cmd
   FOR cmdno = 1 TO getword(2)
    currentindent = stylevars(baseindent)
    outputcmd$ = SPACE$(stylevars(baseindent)) + processcommand$(getword(cmdno + 2)) + newline$
    PUT #tempfile, , outputcmd$
   NEXT
   outputcmd$ = ""

   writescriptender
   PRINT ".";
  END IF
 END IF
NEXT
PRINT
outputcmd$ = ""
END SUB

SUB errordeal (errorstring$, foundstring$)
 PRINT
 COLOR 4
 PRINT "ERROR-"
 PRINT errorstring$;
 COLOR 15
 PRINT foundstring$
 PRINT "[Press any key]"
 CLOSE
 WHILE INKEY$ = "": WEND
 END -1
END SUB

SUB errordeal (errorstring$, foundvalue)
 PRINT
 COLOR 4
 PRINT "ERROR-"
 PRINT errorstring$;
 COLOR 15
 PRINT foundvalue
 PRINT "[Press any key]"
 CLOSE
 WHILE INKEY$ = "": WEND
 END -1
END SUB

SUB warndeal (warnstring$)
 COLOR 4
 PRINT "WARNING: "
 COLOR 15
 PRINT warnstring$
 PRINT "[Press any key]"
 WHILE INKEY$ = "": WEND
END SUB

FUNCTION findoperator (cmdkind, cmdid)
' PRINT "checking cmd kind"; cmdkind; "id"; cmdid; "for being an operator"
 findoperator = 0
 FOR ctr = 0 TO (nooperators - 1)
 ' PRINT "operatorfind counter at"; ctr
  IF operatordata(ctr, operatorid) = cmdid THEN
   IF operatordata(ctr, operatorkind) = cmdkind THEN
    findoperator = ctr + 1   'Return plus 1 so that a no find can be false
  '  PRINT "Found at"; ctr + 1
    EXIT FOR
   END IF
  END IF
 NEXT
END FUNCTION

FUNCTION funcname$ (funcid)
 IF funcid > UBOUND(hardfunction$) THEN
  FOR ii = 0 TO nohighfuncs - 1
   IF highfunctionid(ii) = funcid THEN
    funcname$ = highfunction$(ii)
    EXIT FOR
   END IF
  NEXT
  'want to add some error checking in here if the H. func wasn't found?
 ELSEIF LEN(hardfunction$(funcid)) THEN
  fname$ = hardfunction$(funcid)
  IF LEFT(fname$, 9) = "obsolete_" THEN
   IF RIGHT(fname$, 1) = "*" THEN
    'Warning silenced
    fname$ = LEFT(fname$, LEN(fname$) - 1)
   ELSE
    oldname$ = MID(fname$, 10)
    warndeal "Found use of obsolete command ID " & funcid & " named " & oldname$ & _
             " which is no longer in plotscr.hsd. It might have been renamed, " _
             "removed or replaced with an incompatible command. " _
             "Renaming it to " & fname$ & ". Renaming it to " & _
             oldname$ & " might work."
    hardfunction$(funcid) &= "*"  'silence next time
   END IF
  END IF
  funcname$ = fname$
 ELSE
  warndeal "Found a reference to command " & funcid & ", but it's not in plotscr.hsd"
  funcname$ = "command" & funcid
 END IF
END FUNCTION

FUNCTION getword (wordref)
 IF thisscr.scrformat = 0 THEN
  DIM rtval as SHORT
  GET #currentfile, wordref * 2 + thisscr.codeoffset + pointerblock, rtval
  getword = rtval
 ELSE
  DIM longval as INTEGER
  GET #currentfile, wordref * 4 + thisscr.codeoffset + pointerblock, longval
  getword = longval
 END IF
END FUNCTION

FUNCTION giveconditionclose$ (nocmds)
 IF (nocmds < 2 AND styleoption(simplify1arg)) OR styleoption(conditionnewline) = 0 THEN 'if on newline or split, then go through standerd flow stuff.
  temp$ = ")"
 ELSE
  condsplit = styleoption(conditionsplit) AND (nocmds <> 1)
 ' condnewl = (styleoption(conditionnewline) AND nocmds > 1) AND styleoption(conditionsplit) = 0  'old
  condnewl = styleoption(conditionnewline) AND condsplit = 0
  IF styleoption(flowendnewline) AND condsplit AND condnewl = 0 THEN   'this was the original if  'ok, adjusted.
   IF styleoption(conditionsplit) = 0 THEN temp$ = newline$
   IF styleoption(flowendindented) THEN
    temp$ = temp$ + SPACE$(currentindent)
   ELSE
    temp$ = temp$ + SPACE$(currentindent - stylevars(spacesindent)) 'because current indent already is extra indented
   END IF
  END IF
  IF styleoption(beginendcondition) THEN
   IF styleoption(flowendnewline) OR condnewl OR condsplit THEN  'Orig
   'IF styleoption(conditionsplit) THEN
    temp$ = temp$ + "end"
   ELSE
    temp$ = temp$ + ", end"
   END IF
  ELSE
   temp$ = temp$ + ")"
  END IF
  IF styleoption(newlineafterend) THEN temp$ = temp$ + newline$ + SPACE$(currentindent - stylevars(spacesindent)) 'WATCH THIS
 END IF
 giveconditionclose$ = temp$' + STR$(nocmds)
END FUNCTION

FUNCTION giveconditionopen$ (nocmds)
 IF (nocmds < 2 AND styleoption(simplify1arg)) OR (styleoption(conditionnewline)) = 0 THEN 'if on newline or split, then go through standerd flow stuff.
  temp$ = "("
 ELSE
  IF styleoption(flowbeginnewline) OR styleoption(conditionnewline) THEN
   IF styleoption(flowbeginindented) OR styleoption(conditionnewline) THEN
    temp$ = newline$ + SPACE$(currentindent)
   ELSE
    temp$ = newline$ + SPACE$(currentindent - stylevars(spacesindent)) 'because current indent already is extra indented
   END IF
  END IF
  IF styleoption(beginendcondition) THEN
   IF styleoption(flowbeginnewline) THEN
    temp$ = temp$ + "begin"
   ELSE
    temp$ = temp$ + ", begin"
   END IF
   IF styleoption(conditionnewline) AND (styleoption(conditionsplit) AND nocmds > 1) = 0 THEN temp$ = temp$ + ", "
  ELSE
   temp$ = temp$ + "("
  END IF
 ' IF styleoption(conditionnewline) THEN temp$ = temp$ + newline$
 END IF
 giveconditionopen$ = temp$
END FUNCTION

FUNCTION giveflowclose$ (nocmds)
 'IF nocmds < 2 AND styleoption(simplify1arg) THEN
 ' temp$ = ")"
 'ELSE
  IF styleoption(flowendnewline) THEN
   temp$ = newline$
   IF styleoption(flowendindented) THEN
    temp$ = temp$ + SPACE$(currentindent)
   ELSE
    temp$ = temp$ + SPACE$(currentindent - stylevars(spacesindent))
   END IF
  END IF
  IF styleoption(usebeginendflow) THEN
   IF styleoption(flowendnewline) = 0 THEN temp$ = temp$ + ", "
   temp$ = temp$ + "end"
  ELSE
   temp$ = temp$ + ")"
  END IF
 'END IF
 giveflowclose$ = temp$
END FUNCTION

FUNCTION giveflowopen$ (nocmds) 'I like it a bit better
' IF nocmds < 2 AND styleoption(simplify1arg) THEN    'includes no cmds
 ' temp$ = "("
' ELSE
  IF styleoption(flowbeginnewline) THEN
   IF styleoption(flowbeginindented) THEN
    temp$ = newline$ + SPACE$(currentindent)
   ELSE
    temp$ = newline$ + SPACE$(currentindent - stylevars(spacesindent)) 'because current indent already is extra indented
   END IF
  END IF
  IF styleoption(usebeginendflow) THEN
   IF styleoption(flowbeginnewline) THEN
    temp$ = temp$ + "begin"
   ELSE
    temp$ = temp$ + ", begin"
   END IF
  ELSE
   IF styleoption(flowbeginnewline) = 0 THEN temp$ = temp$ + " "
   temp$ = temp$ + "("
  END IF
  temp$ = temp$ + newline$
' END IF
 giveflowopen$ = temp$
END FUNCTION

SUB includedebug
 COLOR 1: PRINT "Included files debug - ": COLOR 7
 PRINT "No functions (all): "; nofunctions
 PRINT "Highest func id: "; highestfunc
 FOR i = 0 TO highestfunc
  IF i MOD 20 = 0 THEN timelesswait
  PRINT "Func " & i & " is named "; hardfunction$(i)
 NEXT
 PRINT "No high functions: "; nohighfuncs
 FOR i = 0 TO nohighfuncs - 1
  PRINT "H. Func"; i; "has id"; highfunctionid(i); "and is named "; highfunction$(i)
 NEXT
 timelesswait
 PRINT "No operators: "; nooperators
 FOR i = 0 TO nooperators - 1
  PRINT "Op"; i; "is named "; operatorname$(i); " has kind"; operatordata(i, operatorkind); "id"; operatordata(i, operatorid); "value"; operatordata(i, operatorval); "MBUd:"; operatordata(i, opmustbeused)
 NEXT
 timelesswait
 PRINT "No included scripts: "; includedscripts
 FOR i = 0 TO includedscripts - 1
  PRINT "Included script " & i & " is named " & includedscriptname$(i) & " and has id " & includedscriptid(i)
 NEXT
 PRINT "No globals found: "; nofoundglobals
 PRINT
END SUB

FUNCTION intvariable$ (wordref)
 IF getword(wordref) <> 1 THEN errordeal("Int type variable not KIND 1. KIND found was", getword(wordref))
 intvar = getword(wordref + 1)
 IF intvar < 0 THEN
  intvariable$ = returnlocalname$(-1 - intvar)
 ELSE
  intvariable$ = returnglobalname$(intvar)
 END IF
END FUNCTION

SUB Matchscripts
 PRINT "Comparing scripts"
 debug "SCRIPTNAME ----------------------------"
 FOR ctri = 0 TO noscripts - 1
  debug scriptname$(ctri)
 NEXt
 debug "INCLUDEDSCRIPTNAME ---------------------"
 FOR ctrj = 0 TO includedscripts - 1
  debug "'" & includedscriptname$(ctrj) & "' -> '" & normalstring$(includedscriptname$(ctrj)) & "'"
 NEXT
 debug "-----------------------"
 FOR ctri = 0 TO noscripts - 1
  FOR ctrj = 0 TO includedscripts - 1
   IF normalstring$(includedscriptname$(ctrj)) = scriptname$(ctri) THEN
debug includedscriptname$(ctrj) & " include id: " & includedscriptid(ctrj) & " HS id: " & scriptdata(ctri, scriptid)
    IF includedscriptid(ctrj) = scriptdata(ctri, scriptid) THEN
     GOSUB fsin
    ELSE
     IF includedscriptid(ctrj) = -1 AND scriptdata(ctri, scriptid) > 16384 THEN    'Or 16383?
      GOSUB fsin
     ELSE
      IF includedscriptid(ctrj) > 16384 AND scriptdata(ctri, scriptid) > 16384 THEN
       GOSUB fsin
      ELSE
       errordeal(("Script in included file, " + includedscriptname$(ctrj) + " and HS file have the same name but id numbers don't match. Id in HS file was"), scriptdata(ctri, scriptid))
      END IF
     END IF
    END IF
    EXIT FOR
   ELSE
    IF includedscriptid(ctrj) = scriptdata(ctri, scriptid) THEN
     tempyerror$ = "Two scripts ( " + includedscriptname$(ctrj) + " and " + scriptname$(ctri) + " ) have the same id number, but different names."
     errordeal(tempyerror$, "")
    END IF
   END IF
  NEXT
 NEXT
 FOR ctrm = 0 TO includedscripts - 1
  IF includedscript_matched(ctrm) = 0 THEN
   PRINT "Warning- script """; includedscriptname$(ctrm); """ was found in an included file, but not the HS file."
   PRINT "This probably means that the version of plotscr.hsd isn't the one you compiled with. Turning off warning."
   EXIT FOR
  END IF
 NEXT
 GOTO quitoutta

fsin:             
   hsscript_matched(ctri) = 1
   includedscript_matched(ctrj) = 1
   RETURN

quitoutta:

END SUB

SUB matchscriptsdebug
 COLOR 1: PRINT "Compare scripts debug - ": COLOR 7
 PRINT "No HS scripts:"; noscripts; "No included scripts:"; includedscripts
 FOR op = 0 TO noscripts - 1
  IF op MOD 20 = 0 THEN timelesswait
  PRINT "HS script no"; op; "was found:"; hsscript_matched(op)
 NEXT
 FOR op = 0 TO includedscripts - 1
  PRINT "Included script no"; op; "was found:"; includedscript_matched(op)
 NEXT
END SUB

FUNCTION normalizestring$ (startstring$)
' IF styleoption(normalizefontcapitals) THEN    'deactivated feature- doesn't work
 ' instring$ = UCASE$(LTRIM$(RTRIM$(instring$)))
 'ELSE
  instring$ = startstring$
  instring$ = LCASE$(LTRIM$(RTRIM$(instring$)))
' END IF
 lenstr = LEN(instring$)
 outstring$ = ""
 result = -1
 DO UNTIL result = 0
  result = INSTR(instring$, " ")
  IF result > 0 THEN
   outstring$ = outstring$ + MID$(instring$, 1, result - 1)
   instring$ = MID$(instring$, result + 1)
  END IF
 LOOP
 outstring$ = outstring$ + instring$
 normalizestring$ = outstring$
END FUNCTION

'remove all spaces and lower case, normalising it
FUNCTION normalstring$ (startstring$)     'for when I need to get the actual scritpname as appears in the hs
 instring$ = startstring$
 instring$ = LCASE$(LTRIM$(RTRIM$(instring$)))
 lenstr = LEN(instring$)
 outstring$ = ""
 result = -1
 DO UNTIL result = 0
  result = INSTR(instring$, " ")
  IF result > 0 THEN
   outstring$ = outstring$ + MID$(instring$, 1, result - 1)
   instring$ = MID$(instring$, result + 1)
  END IF
 LOOP
 outstring$ = outstring$ + instring$
 normalstring$ = outstring$
END FUNCTION

SUB optionsselection
 
 IF hsfile$ = "" THEN INPUT "HS file to decompile"; hsfile$
 IF noincludes = 0 THEN
  INPUT "Number of files to include"; noincludes
  IF noincludes > 8 THEN PRINT "Too many, redo"
  IF noincludes < 1 THEN PRINT "Need at least 1 include file"
  FOR i = 0 TO noincludes - 1
   PRINT "Name of include no"; (i + 1);
   INPUT includenames$(i)
  NEXT
 END IF
 IF styleoption(noformatneeded) = 0 THEN
  PRINT "Load which set of formatting options? (leave blank to load a blank set)"
  PRINT "You may view, change and save the options afterwards.  Builtin option sets are:   default  C  clearcut  theworks"
  temp = TIMER * 1000
  COLOR 14: INPUT "    ", fname$: COLOR 7
  starttime = starttime + (TIMER * 1000 - temp)
  IF fname$ <> "" THEN
   readformatfile(fname$)
   PRINT "View and change options? (y/n)"
   temp = TIMER * 1000
   COLOR 14: INPUT "    ", answer$: COLOR 7
   starttime = starttime + (TIMER * 1000 - temp)
   IF normalstring$(answer$) <> "y" THEN EXIT SUB
  END IF
  GOSUB clearpage
  pageno = 1
  PRINT
  baseline = CSRLIN - 15
  LOCATE baseline
  PRINT
  temp = TIMER * 1000
  PRINT "Variable setting - "
  PRINT "Number of spaces to indent top level statements?    (Is"; stylevars(defineindent); ")"
  COLOR 14: INPUT " ", stylevars(defineindent): COLOR 7
  PRINT "Number of spaces to indent first line of commands?    (Is"; stylevars(baseindent); ")"
  COLOR 14: INPUT " ", stylevars(baseindent): COLOR 7
  PRINT "Number of spaces to indent each additional indentation of commands?    (Is"; stylevars(spacesindent); ")"; "  "
  COLOR 14: INPUT " ", stylevars(spacesindent): COLOR 7
  starttime = starttime + (TIMER * 1000 - temp)
  LOCATE baseline
  GOSUB clearpage
  PRINT " Press the number to change that option"
  PRINT
 ' GOSUB page1
  WHILE pageno < 5
 '  tuu = CSRLIN - 14
   'PRINT "LL: "; CSRLIN
   LOCATE baseline
  ' PRINT "woo"
   GOSUB clearpage
'   PRINT "wee"
   LOCATE baseline
   SELECT CASE pageno
   CASE 1: GOSUB page1
   CASE 2: GOSUB page2
   CASE 3: GOSUB page3
   CASE 4: GOSUB page4
   END SELECT
   LOCATE baseline
   FOR i = 0 TO noops - 1
   ' LOCATE (baseline + 2 + i), 40
    LOCATE CSRLIN, 75
    IF styleoption(startop + i) THEN
     COLOR 10: PRINT "ON ": COLOR 7
    ELSE
     COLOR 12: PRINT "OFF": COLOR 7
    END IF
   NEXT
   LOCATE CSRLIN + 2, 1
   temp = TIMER * 1000
   PRINT "Option selection"
   COLOR 14: INPUT "    ", opsel: COLOR 7
   starttime = starttime + (TIMER * 1000 - temp)
   IF opsel = 0 THEN pageno = pageno + 1
   IF opsel <= noops AND opsel > 0 THEN
    LOCATE baseline + opsel + 2, 60
    IF styleoption(startop + opsel - 1) THEN
     setstyleoption(startop + opsel - 1, 0)
     IF startop + opsel - 1 = conditionnewline THEN setstyleoption(conditionsplit, 0)   'Save me some damn trouble
    ELSE
     setstyleoption(startop + opsel - 1, 1)
     IF startop + opsel - 1 = conditionsplit THEN setstyleoption(conditionnewline, 1)   'Save me some damn trouble
    END IF
   END IF
  WEND
  PRINT "Save formating options? (Leave blank to not save, otherwise type up to 8 letter NAME)"
  temp = TIMER * 1000
  COLOR 14: INPUT "    ", fname$: COLOR 7
  starttime = starttime + (TIMER * 1000 - temp)
  IF normalstring$(fname$) <> "" THEN writeformatfile(fname$)

 ' setstyleoption(renumberautonos, 1)
 ' setstyleoption(usedscriptcons, 1)
 ' setstyleoption(useoperators, 1)
 ' setstyleoption(indentautonoscripts, 1)
 '  stylevars(baseindent) = 1
 '  stylevars(spacesindent) = 2
 '  stylevars(defineindent) = 1
  'setstyleoption(sortglobals, 1)
' PRINT "useoperators is"; styleoption(useoperators)
 'INPUT "Use found names? (1 or 0) ", foundn
 'setstyleoption(usefoundname, foundn)
 ' setstyleoption(usefoundname, 1)
 END IF


 GOTO endthissub

clearpage:
 FOR i = 0 TO 13: PRINT SPACE$(80): NEXT
 RETURN

page1:
 noops = 6
 startop = 1
 PRINT "1  Use 'begin' and 'end' for toplevel declarations"
 PRINT "2  Define each global in a seperate 'global variable'"
 PRINT "3  Define each script in a seperate 'define script'"
 PRINT "4  Rewrite autonumber scripts with id -1 (may affect autono ids if on)"
 PRINT "5  Indent autonumber scripts"
 PRINT "6  Output a separator between scripts"
 PRINT "0 or Enter -Go to next page"
 PRINT
' INPUT "Option selection"; opselc
 RETURN

page2:
 noops = 5
 startop = 10
 PRINT "1  Start naming variables from local1, instead of local0"
 PRINT "2  Define each variable in a separate 'variable' statement"
 PRINT "3  Don't indent 'variable' statements"
 PRINT "4  Name args with the name 'argx' rather than 'localx'"
 PRINT "5  Write a blank line after 'variable' blocks/statements"
 PRINT "0 or Enter -Go to next page"
 PRINT
' INPUT "Option selection"; opselc
 RETURN

page3:
 noops = 9
 startop = 20
 PRINT "1  Use 'begin' and 'end' in flow statements"
 PRINT "2  Put begin & ( on a new line for flow"
 PRINT "3  Extra indent begin & ( like cmds for flow"
 PRINT "4  Put end & ) on a new line for flow"
 PRINT "5  Extra indent end & ) like cmds for flow"
 PRINT "6  Newline after end & ) in all cases, eg 'then'"
 PRINT "7  Use 'begin' and 'end' for conditions"
 PRINT "8  Write conditions on a newline"
 PRINT "9  Split up multiple condition values to 1 per line (turns above on)"
 PRINT "0 or Enter -Go to next page"
 PRINT
' INPUT "Option selection"; opselc
 RETURN

page4:
 noops = 7
 startop = 30
 PRINT "1  Sort globals into order by id for 'global variable'"
' PRINT "2  Normalize strings into capitals instead of lowercase"
 PRINT "2  Use names as found in include files instead of normalized"
 PRINT "3  Use operators (read from include files)"
 PRINT "4  Use the constants 'none' and 'autonumber' in definescript blocks"
 PRINT "5  Always use 'set variable' (overrides operators)"
 PRINT "6  Skip the 'for' step amount if 1"
 PRINT "7  Simplify all flow and conditions with only 1 cmd/arg. "
 PRINT "          Uses brackets and no newlines. (Overrides everything else)"
 PRINT "0 or Enter -Finish options selection"
 PRINT
' INPUT "Option selection"; opselc
 RETURN
 
'CONST usebeginendtoplevel = 1, separateglobals = 2, separatescripts = 3 ' topleveldontdoflow just prints clean brackets or begin, end
'CONST renumberautonos = 4, indentautonoscripts = 5, linebetweenscripts = 6
'CONST writesindorder = 7, writedinsorder = 8, sortscripts = 9 'delete this?

'CONST namevarsfrom1 = 10, separatevariables = 11, dontindentvars = 12, nameargsseparate = 13, lineaftervariables = 14

'CONST usebeginendflow = 20, flowbeginnewline = 21, flowbeginindented = 22  'the flow begin, end newline, indented used for conditions too.
'CONST flowendnewline = 23, flowendindented = 24, newlineafterend = 25
'CONST beginendcondition = 26, conditionnewline = 27, conditionsplit = 28

'CONST sortglobals = 30,  usefoundname = 31, useoperators = 32, usedscriptcons = 33
'CONST usesetvariable = 34, missforstep = 35, simplify1arg = 36 'for cmds and conditions
' , normalizefontcapitals = 37

updateandwait:

  RETURN

endthissub:

END SUB

SUB outputthefile
 PRINT "Writing output file"

' separater$ = "#-----------------------------" + newline$ + newline$
' outputfile = 131

 'open the file
  outputfile = 74
 IF hssfile$ = "" THEN 'figure out the name fo the outputfile
  tempnum = INSTR(hsfile$, ".")
  hssfile$ = MID$(hsfile$, 1, tempnum - 1) + ".hss"
'  PRINT "Gonna make "; hssfile$
 END IF
 OPEN hssfile$ FOR BINARY AS outputfile
 IF LOF(outputfile) > 0 THEN
  PRINT "Hss file ";
  COLOR 15: PRINT hssfile$; : COLOR 7: PRINT " already exists. Overwrite? (y/n)"
  PRINT "    ";
  answer$ = INKEY$   'This is timeleswait modified
  temp = TIMER * 1000
  WHILE answer$ = "": answer$ = INKEY$: WEND
  starttime = starttime + (TIMER * 1000 - temp)
  IF LCASE$(answer$) = "y" THEN
   COLOR 14: PRINT "y": COLOR 7
   CLOSE outputfile
   KILL hssfile$
   OPEN hssfile$ FOR BINARY AS outputfile
  ELSE
   PRINT "Bye then"
   CLOSE
   END -1
  END IF
 END IF


' PRINT "outfile:"; outputfile

 'write header
 header$ = "# Plotscripts decompiled from " + hsfile$ + newline$
 PUT #outputfile, , header$
 header$ = ""

 'write includes
 outputcmd$ = ""
 FOR ii = 0 TO noincludes - 1
  outputcmd$ = outputcmd$ + "include, """ + includenames$(ii) + """" + newline$
 NEXT
 outputcmd$ = outputcmd$ + newline$
 PUT #outputfile, , outputcmd$

 'write scripts defines to outputcmd$
 outputcmd$ = ""
 firstscript = 1
 i = 0
 wrotescripts = 0
'  PRINT ":P"
 DO WHILE i < noscripts
  IF hsscript_matched(i) = 0 THEN 'If it wasn't in an include, define it
   IF styleoption(renumberautonos) AND (scriptdata(i, scriptid) > 16383) THEN
    IF styleoption(usedscriptcons) THEN
     displayid$ = "autonumber"
    ELSE
     displayid$ = "-1"
    END IF
   ELSE
    displayid$ = LTRIM$(STR$(scriptdata(i, scriptid)))
   END IF
   IF scriptdata(i, scriptargs) = 0 AND styleoption(usedscriptcons) THEN
    displayargs$ = "none"
   ELSE
    displayargs$ = LTRIM$(STR$(scriptdata(i, scriptargs)))
   END IF
   IF styleoption(indentautonoscripts) AND (scriptdata(i, scriptid) > 16383) THEN outputcmd$ = outputcmd$ + SPACE$(stylevars(spacesindent))
   IF styleoption(separatescripts) OR firstscript THEN
    outputcmd$ = outputcmd$ + "define script"
    IF styleoption(usebeginendtoplevel) THEN
     outputcmd$ = outputcmd$ + ", begin" '+ newline$
     IF styleoption(separatescripts) THEN outputcmd$ = outputcmd$ + ", "
    ELSE
     outputcmd$ = outputcmd$ + " ("
    END IF
    IF styleoption(separatescripts) = 0 AND decmplscripts <> 1 THEN outputcmd$ = outputcmd$ + newline$ + SPACE$(stylevars(defineindent))
    outputcmd$ = outputcmd$ + displayid$ + ", " + scriptname$(i) + ", " + displayargs$
    FOR j = 1 TO scriptdata(i, scriptargs)
     outputcmd$ = outputcmd$ + ", " + "0"
    NEXT
    IF styleoption(separatescripts) OR wrotescripts + 1 = decmplscripts THEN
     IF styleoption(usebeginendtoplevel) THEN
      outputcmd$ = outputcmd$ + ", end" + newline$
     ELSE
      outputcmd$ = outputcmd$ + ")" + newline$
     END IF
    ELSE
     outputcmd$ = outputcmd$ + newline$
    END IF
   ELSE
    outputcmd$ = outputcmd$ + SPACE$(stylevars(defineindent)) + displayid$ + ", " + scriptname$(i) + ", " + displayargs$
    FOR j = 1 TO scriptdata(i, scriptargs)
     outputcmd$ = outputcmd$ + ", " + "0"
    NEXT
    'IF i = noscripts THEN
    outputcmd$ = outputcmd$ + newline$
   END IF
 '  firstscript = 0
 '  i = i + 1
 '  IF i = noscripts OR (styleoption(separatescripts) = 0) THEN
''' IF wrotescripts = decmplscripts AND (styleoption(separatescripts) = 0) THEN
  '  IF styleoption(usebeginendtoplevel) THEN
  '   outputcmd$ = outputcmd$ + newline$ + "end" + newline$
  '  ELSE
  '   outputcmd$ = outputcmd$ + ")" + newline$
  '  END IF
  ' ELSE  
  '  outputcmd$ = outputcmd$ + newline$
''' END IF
   firstscript = 0
   wrotescripts = wrotescripts + 1
  END IF
  i = i + 1
 LOOP
 IF styleoption(usebeginendtoplevel) THEN
  outputcmd$ = outputcmd$ + "end" + newline$
 ELSE
  outputcmd$ = outputcmd$ + ")" + newline$
 END IF

 outputcmd$ = outputcmd$ + newline$
 PUT #outputfile, , outputcmd$      'write the script defines to the hss

 'write global variables to the hss
 outputcmd$ = ""
 FOR i = 0 TO noaddglobals - 1                                 'THIS SECTION IS STUFFED
  IF styleoption(separateglobals) OR i = 0 THEN
   outputcmd$ = outputcmd$ + "global variable"
   IF styleoption(usebeginendtoplevel) THEN
    outputcmd$ = outputcmd$ + ", begin" + newline$
   ELSE
    outputcmd$ = outputcmd$ + " ("
    IF noaddglobals <> 1 AND styleoption(separateglobals) = 0 THEN outputcmd$ = outputcmd$ + newline$
   END IF
   IF styleoption(separateglobals) = 0 THEN outputcmd$ = outputcmd$ + SPACE$(stylevars(defineindent))
   outputcmd$ = outputcmd$ + LTRIM$(STR$(globalsdec(i))) + ", " + "global" + LTRIM$(STR$(globalsdec(i)))
   IF styleoption(separateglobals) OR i = noaddglobals - 1 THEN
    IF styleoption(usebeginendtoplevel) THEN
     outputcmd$ = outputcmd$ + newline$ + "end" + newline$
    ELSE
     outputcmd$ = outputcmd$ + ")" + newline$
    END IF
   ELSE
    outputcmd$ = outputcmd$ + newline$
   END IF
  ELSE
   outputcmd$ = outputcmd$ + SPACE$(stylevars(defineindent)) + LTRIM$(STR$(globalsdec(i))) + ", " + "global" + LTRIM$(STR$(globalsdec(i)))
   IF i = noaddglobals - 1 THEN
    IF styleoption(usebeginendtoplevel) THEN
     outputcmd$ = outputcmd$ + newline$ + "end" + newline$
    ELSE
     outputcmd$ = outputcmd$ + newline$ + ")" + newline$
    END IF
   ELSE
    outputcmd$ = outputcmd$ + newline$
   END IF
  END IF
 NEXT
 IF noaddglobals THEN outputcmd$ = outputcmd$ + newline$
 PUT #outputfile, , outputcmd$      'write the global defines to the hss

 'write scripts to the hss
 SEEK tempfile, 1
 bigbuffer$ = SPACE$(50)
 lilbuffer$ = " "
 WHILE SEEK(tempfile) < LOF(tempfile)           'Copy the temp file
  IF SEEK(tempfile) + 50 < LOF(tempfile) THEN
   GET #tempfile, , bigbuffer$
   PUT #outputfile, , bigbuffer$
  ELSE
   GET #tempfile, , lilbuffer$
   PUT #outputfile, , lilbuffer$
  END IF
 WEND
 CLOSE tempfile
 KILL "HSDCMP00.TMP"

 CLOSE outputfile
END SUB

SUB printoutput
 PRINT
 PRINT "Read"; nofunctions; " functions,"; includedscripts; " scripts,"; nofoundglobals; " globals and"; nooperators; " operators from included files"
 PRINT "Found"; noscripts; " scripts in ";
 COLOR 15: PRINT hsfile$: COLOR 7
 PRINT "Decompiled"; decmplscripts; " scripts, defined"; noaddglobals; " globals and added"; noincludes; " files to ";
 COLOR 15: PRINT hssfile$: COLOR 7
END SUB

FUNCTION processcommand$ (wordref, parentrule, argnum)  'Not finished! Operators work
 cmdkind = getword(wordref)
 cmdid = getword(wordref + 1)
 processcommand$ = ""
' PRINT "Processing command kind"; cmdkind; "id is"; cmdid
 IF cmdkind > 7 OR cmdkind < 1 THEN errordeal("Illegal cmd KIND value read. KIND read was", cmdkind)
 IF cmdkind = 1 THEN
  IF parentrule > 0 THEN
   processcommand$ = getconstantfromrule$(funcrules(parentrule, argnum) - 1, cmdid)
  ELSE
   processcommand$ = STR$(cmdid)
  END IF
 END IF
 IF cmdkind = 2 THEN processcommand$ = processflow$(wordref, 0)
 IF cmdkind = 3 THEN processcommand$ = returnglobalname$(cmdid)
 IF cmdkind = 4 THEN processcommand$ = returnlocalname$(cmdid)
 IF cmdkind > 4 THEN
  tempop = findoperator(cmdkind, cmdid)
 ' PRINT "op found was"; tempop
 ' PRINT "tempop is"; tempop;" and next is"; (operatordata(tempop, opmustbeused) OR styleoption(useoperators))
 ' PRINT "logic is"; tempop AND (operatordata(tempop, opmustbeused) OR styleoption(useoperators))
  IF tempop AND (operatordata(tempop, opmustbeused) OR styleoption(useoperators)) AND (cmdkind <> 5 OR cmdid <> 16 OR styleoption(usesetvariable) = 0) THEN
   processcommand$ = processoperator$(wordref, tempop - 1) '-1 because findoperator adds one to the result
  ELSE
   'treat these commands specially for strings
   IF cmdkind = 6 AND (cmdid = 251 OR cmdid = 252) THEN
    processcommand$ = processnewstring$(cmdid, wordref)
    EXIT FUNCTION
   END IF
   IF cmdkind = 7 AND (cmdid = setstring OR cmdid = appendstring) THEN
    ret$ = processoldstring$(cmdid, wordref)
    IF LEN(ret$) THEN processcommand$ = ret$ : EXIT FUNCTION
   END IF
   constantrule = -1
   IF cmdkind = 5 THEN temp$ = mathfuncname$(cmdid)
   IF cmdkind = 6 THEN
    IF funcrules(cmdid, 0) THEN constantrule = cmdid
    temp$ = funcname$(cmdid)   'a function
   END IF
   IF cmdkind = 7 THEN temp$ = scriptname$(returnscriptdata(arrayno, cmdid))
  'Process arguments - only kinds 6, 7, and 5 because flow has a separate function
   argcount = getword(wordref + 2)
   IF argcount > 0 THEN
    temp$ = temp$ + " ("                        '''''''''''##########################################################################
    IF cmdkind = 5 AND (cmdid >= 15 AND cmdid <= 18) THEN 'get the name of the local as first arg
     temp$ = temp$ + intvariable$(getword(wordref + 3)) + ", " + processcommand$(getword(wordref + 4))
    ELSE
     FOR ctr = 1 TO argcount
      temp$ = temp$ + processcommand$(getword(wordref + ctr + 2), constantrule, ctr)
      IF ctr <> argcount THEN temp$ = temp$ + ", "
     NEXT
    END IF
    temp$ = temp$ + ")"
   END IF
   processcommand$ = temp$
  END IF
 END IF
END FUNCTION

FUNCTION processflow$ (wordref, canENL, attached_to_if = 0)
 flowtype = getword(wordref + 1)
 indent = currentindent
 noargs = getword(wordref + 2)
 currentindent = currentindent + stylevars(spacesindent)
 'canENL = 0   'can put a newline after an end
 'PRINT "Current indent is"; currentindent; " flowtype is"; flowtype
 SELECT CASE flowtype
  CASE 0  'do
'   currentindent = currentindent + stylevars(spacesindent)
   temp$ = "do"
   GOSUB docmdlist
  CASE 3    'Return
 '  currentindent = currentindent + stylevars(spacesindent)
   temp$ = "return"
   GOSUB docmdlist
  CASE 4  'if
'   currentindent = currentindent + stylevars(spacesindent)
   temp$ = "if " + giveconditionopen$(1)
  ' IF styleoption(conditionnewline) AND styleoption(simplify1arg) = 0 THEN temp$ = temp$ + SPACE$(currentindent)
   temp$ = temp$ + processcommand$(getword(wordref + 3)) + giveconditionclose$(1)
   currentindent = indent
   IF styleoption(newlineafterend) = 0 OR styleoption(conditionnewline) = 0 OR styleoption(simplify1arg) THEN
    IF styleoption(beginendcondition) AND styleoption(simplify1arg) = 0 THEN temp$ = temp$ + ", " ELSE temp$ = temp$ + " "
   END IF
   thereisthen = getword(getword(wordref + 4) + 2)
   thereiselse = getword(getword(wordref + 5) + 2) 'number of cmds else has
   thenkind = getword(getword(getword(wordref + 4) + 3)) 'kind of thens first argument. Only valid if then has at least one argument.
   IF thereiselse THEN temppyno = 1
   IF thereisthen THEN
    temp$ = temp$ + processflow$(getword(wordref + 4), temppyno, -1) 'then
    IF thereiselse THEN
     IF styleoption(newlineafterend) AND thereisthen THEN
      temp$ = temp$ + newline$ + SPACE$(currentindent)
     ELSE
      IF styleoption(usebeginendflow) AND (thereisthen = 1 AND styleoption(simplify1arg) AND thenkind <> 2) = 0 THEN temp$ = temp$ + ", " ELSE temp$ = temp$ + " "  'moved down to else
     END IF
    END IF
   END IF
   temp$ = temp$ + processflow$(getword(wordref + 5), 0, -1) 'else
  CASE 5  'then
   IF noargs THEN
    temp$ = "then"  'note the space   NOTE IT  'I DELETED THE SPACE... DONT BLOW UP ON ME!@
    IF attached_to_if = 0 THEN temp$ = !"do  # Bug in your script: was a floating 'then'\n"
    GOSUB docmdlist
   ELSE
    temp$ = ""
    IF attached_to_if = 0 THEN temp$ = !"do()  # Bug in your script: was a floating 'then'\n"
   END IF
  CASE 6 'else
   IF noargs THEN
    IF canENL THEN
     IF styleoption(usebeginendflow) THEN temp$ = ", "  ' ELSE temp$ = " "
    END IF
    IF attached_to_if THEN
     temp$ = temp$ + "else"
    ELSE
     temp$ = temp$ + !"do  # Bug in your script: was a floating 'else'\n"
    END IF
    GOSUB docmdlist
   ELSE
    temp$ = ""
    IF attached_to_if = 0 THEN temp$ = !"do()  # Bug in your script: was a floating 'else'\n"
   END IF
  CASE 7 'for
   temp$ = "for " + giveconditionopen$(5)
   IF styleoption(conditionsplit) THEN
    firsep$ = newline$ + SPACE$(currentindent)  'done in open condit?  'I don't think so
    firstep$ = ""
    restsep$ = SPACE$(currentindent)
    ifnewline$ = newline$
   ELSE
   ' IF styleoption(conditionnewline) THEN temp$ = temp$ + newline$ + SPACE$(currentindent) 'done in open condit?
    firsep$ = ""
    restsep$ = ", "
    ifnewline$ = ""
   END IF
   temp$ = temp$ + firsep$ + intvariable$(getword(wordref + 3)) + ifnewline$
   FOR vy = 4 TO 5
    temp$ = temp$ + restsep$ + processcommand$(getword(wordref + vy)) + ifnewline$
   NEXT
   IF styleoption(missforstep) = 0 OR getword(getword(wordref + 6)) <> 1 OR getword(getword(wordref + 6) + 1) <> 1 THEN
    temp$ = temp$ + restsep$ + processcommand$(getword(wordref + 6)) + ifnewline$
   END IF
  ' IF styleoption(conditionsplit) THEN
 '  IF styleoption(conditionnewline) THEN
 '   IF ifnewline$ = "" THEN temp$ = temp$ + newline$
                    'FIIINN!N!NN###################################333333333333 I thinnk
'   ELSE
    temp$ = temp$ + giveconditionclose$(5)
 '  END IF

   ' IF styleoption(flowendindented) THEN     'this bit is all (hopefully) in conditionclose
   '  temp$ = temp$ + SPACE$(currentindent)
   ' ELSE
   '  temp$ = temp$ + SPACE$(indent)
   ' END IF
  '  IF styleoption(beginendcondition) THEN
  '   temp$ = temp$ + "end"
  '  ELSE
  '   temp$ = temp$ + ")"
  '  END IF
 '  ELSE
 '   temp$ = temp$ + giveconditionclose$(noargs)
'   END IF
  ' IF styleoption(newlineafterend) THEN      'this bit is in conditionclose 
  '  temp$ = temp$ + newline$ + SPACE$(indent)
  ' ELSE
    IF styleoption(newlineafterend) = 0 OR styleoption(conditionnewline) = 0 THEN
     IF styleoption(beginendcondition) THEN temp$ = temp$ + ", " ELSE temp$ = temp$ + " "   'this has been changed
    END IF
  ' END IF
   currentindent = indent
   temp$ = temp$ + processflow$(getword(wordref + 7), 0)
  CASE 10 'while
   temp$ = "while " + giveconditionopen$(1)
   condnewl = styleoption(conditionnewline) AND styleoption(simplify1arg) = 0
 '  IF condnewl THEN temp$ = temp$ + SPACE$(currentindent)
   temp$ = temp$ + processcommand$(getword(wordref + 3)) + giveconditionclose$(1)
   IF (styleoption(newlineafterend) AND styleoption(conditionnewline)) = 0 OR styleoption(simplify1arg) THEN
    IF styleoption(beginendcondition) AND styleoption(simplify1arg) = 0 THEN temp$ = temp$ + ", " ELSE temp$ = temp$ + " "
   END IF
   currentindent = indent
   temp$ = temp$ + processflow$(getword(wordref + 4), 0)
  CASE 11    'break
   temp$ = "break"
   GOSUB docmdlist
  CASE 12    'continue
   temp$ = "continue"
   GOSUB docmdlist
  CASE 13    'exit script
   temp$ = "exit script"
   GOSUB docmdlist
  CASE 14    'exit Returning
   temp$ = "exit returning"
   GOSUB docmdlist
  CASE ELSE
   errordeal("Flow command type in HS that does not exist. Flow id was:", flowtype)
 END SELECT
 processflow$ = temp$
 currentindent = indent 'this is NOT ANYMORE! done in docmdlist
 GOTO endthisfunc

docmdlist:

  IF noargs = 1 AND styleoption(simplify1arg) AND getword(getword(wordref + 3)) <> 2 THEN ' is not a flow
   temp$ = temp$ + " (" + processcommand$(getword(wordref + 3)) + ")"
  ELSE
   temp$ = temp$ + giveflowopen$(noargs)
   FOR ctr = 1 TO noargs - 1
    temp$ = temp$ + SPACE$(currentindent) + processcommand$(getword(wordref + 2 + ctr)) + newline$
   NEXT
   IF styleoption(flowendnewline) = 0 THEN 'for the last cmd
    temp$ = temp$ + SPACE$(currentindent) + processcommand$(getword(wordref + 2 + noargs))
    IF styleoption(usebeginendflow) THEN temp$ = temp$ + ", "
   ELSE
    temp$ = temp$ + SPACE$(currentindent) + processcommand$(getword(wordref + 2 + noargs)) ' + newline$
   END IF
   temp$ = temp$ + giveflowclose$(noargs)
  END IF
  IF canENL AND styleoption(newlineafterend) THEN temp$ = temp$ + SPACE$(currentindent)
  'Note: no newline added to end because the flow control that triggered this does that.
  RETURN

endthisfunc:
END FUNCTION

FUNCTION processoperator$ (wordref, id) 'Doney. Some of the double pointers might cause trouble though
 opname$ = operatorname$(id) 'SERIOUS NOTE: Check for useops switch if not a must have op
' PRINT "process ops. op name is "; opname$; " Stack left is"; FRE(-2)
 FOR ctr = 0 TO UBOUND(builtinop$)
  IF opname$ = builtinop$(ctr) THEN
   nocommas = 1
   EXIT FOR
  END IF
 NEXT
 mainopval = operatordata(id, operatorval)
 op1kind = getword(getword(wordref + 3))
 op1id = getword(getword(wordref + 3) + 1)
 op2kind = getword(getword(wordref + 4))
 op2id = getword(getword(wordref + 4) + 1)
 opid1 = findoperator(op1kind, op1id)
 opid2 = findoperator(op2kind, op2id)
' PRINT "left cmd:  kind is"; op1kind; "and id is"; op1id; "  right cmd: kind is"; op1kind; "and id is"; op1id
' PRINT "opid1 is"; opid1; "and 2 is"; opid2
 IF opid1 AND (operatordata(tempop, opmustbeused) OR styleoption(useoperators)) THEN
  IF operatordata(opid1 - 1, operatorval) > mainopval THEN
   temp$ = "(" + processoperator$(getword(wordref + 3), opid1 - 1) + ")"
  ELSE
   temp$ = processoperator$(getword(wordref + 3), opid1 - 1)
  END IF
 ELSE
  IF operatordata(id, operatorkind) = 5 AND operatordata(id, operatorid) >= 16 AND operatordata(id, operatorid) <= 18 THEN 'if the first arg is a variable
   temp$ = intvariable$(getword(wordref + 3))
  ELSE
   temp$ = processcommand$(getword(wordref + 3))
  END IF
 END IF
 IF nocommas THEN
  temp$ = temp$ + " " + opname$ + " "
 ELSE
  temp$ = temp$ + ", " + opname$ + ", "
 END IF
' PRINT "now testing 2nd cmd"
 IF opid2 AND (operatordata(tempop, opmustbeused) OR styleoption(useoperators)) THEN
  IF operatordata(opid2 - 1, operatorval) >= mainopval THEN
   temp$ = temp$ + "(" + processoperator$(getword(wordref + 4), opid2 - 1) + ")"
  ELSE
   temp$ = temp$ + processoperator$(getword(wordref + 4), opid2 - 1)
  END IF
 ELSE
  temp$ = temp$ + processcommand$(getword(wordref + 4))
 END IF
 processoperator$ = temp$
END FUNCTION

SUB readrulefile
 PRINT "Reading constant matching file"
 REDIM funcrules(0 TO 1000, 0 TO 16)
 REDIM constantlookup(0)
 DIM temp$

 debug "ubound = " & UBOUND(constantlookup)

 fh = FREEFILE
 IF OPEN(EXEPATH & "/rules.txt" FOR INPUT AS #fh) THEN
  IF OPEN("rules.txt" FOR INPUT AS #fh) THEN
   COLOR 15
   PRINT " WARNING: rules.txt is missing. Decompile results will be difficult to read."
   COLOR 7
   EXIT SUB
  END IF
 END IF
 WHILE NOT EOF(fh)
  state = 1
  ruleprops = 0
  rulenum = UBOUND(constantlookup)

  LINE INPUT #fh, temp$
  temp$ = MID$(temp$, 1, INSTR(temp$, "#") - 1)

  WHILE temp$ <> ""
   ctr = INSTR(temp$, " ")
   IF ctr THEN
    word$ = normalizestring$(MID$(temp$, 1, ctr))
    temp$ = MID$(temp$, ctr + 1)
   ELSE
    word$ = normalizestring$(temp$)
    temp$ = ""
   END IF
   IF word$ = "" THEN CONTINUE WHILE

   debug "word$ = '" & word$ & "' rule = " & rulenum

   IF word$ = "=>" THEN
    state = 2
   ELSEIF word$ = "$bitmask" THEN
    ruleprops = 1000000
   ELSEIF state = 1 THEN
    comma = INSTR(word$, ",")
    func$ = MID$(word$, 1, comma - 1)
    argno = VAL(MID$(word$, comma + 1))
    debug "funcname = '" & func$ & "' argno = " & argno
    FOR i = 0 TO highestfunc
     IF normalizestring$(hardfunction$(i)) = func$ THEN
      debug "function '" & hardfunction$(i) & "' matches"
      funcrules(i, 0) = -1
      funcrules(i, argno) = rulenum + ruleprops + 1
      EXIT FOR
     END IF
    NEXT
   ELSE
'    constantlookup(rulenum).table =
    FOR i = 0 TO UBOUND(constants) - 1
     IF MID$(word$, LEN(word$)) = ":" THEN
      cond = MID$(constants(i).str, 1, LEN(word$)) = word$
     ELSE
      cond = constants(i).str = word$
     END IF
     IF cond THEN
      debug "'" & constants(i).str & "' matches this"
      IF styleoption(usefoundname) THEN
       addconstanttorule(rulenum, constants(i).real, constants(i).value)
      ELSE
       addconstanttorule(rulenum, constants(i).str, constants(i).value)
      END IF
     END IF
    NEXT
   END IF
  WEND

  rulenum += 1
  REDIM PRESERVE constantlookup(rulenum)
 WEND

 CLOSE #fh
END SUB

SUB readallincludes
 PRINT "Reading include files"
 REDIM constants(100)
 REDIM hardfunction$(0 TO 1000)
 REDIM operatordata(40, 0 TO 3)
 REDIM operatorname$(40)
 REDIM tempopreal$(100)
 REDIM includedscriptid(30), includedscriptname$(30)
 'include each file here
 FOR i = 0 TO (noincludes - 1)
  includefile = FREEFILE
  PRINT "Including ";
  COLOR 15: PRINT includenames$(i): COLOR 7
  IF OPEN(EXEPATH & "/" & includenames$(i) FOR BINARY ACCESS READ AS includefile) THEN
   IF OPEN(includenames$(i) FOR BINARY ACCESS READ AS includefile) THEN
    errordeal("File not found: " & includenames$(i), "")
   END IF
  END IF
  'PRINT "size file is"; LOF(includefile)
  readinclude
  CLOSE includefile
 NEXT
 'cross off the scripts in the includes from the list that need to be decompiled

 FOR ctr = 0 TO (nooperators - 1)  'Go through all the operators and try and find a func or script with same name.
  realname$ = tempopreal$(ctr)
 ' PRINT "-"; realname$; "-"
  foundequal = 0
  FOR ctr2 = 0 TO UBOUND(mathfuncname$)
   IF mathfuncname$(ctr2) = realname$ THEN
    operatordata(ctr, operatorkind) = 5
    operatordata(ctr, operatorid) = ctr2
    foundequal = 1
    EXIT FOR
   END IF
  NEXT
  IF foundequal = 0 THEN
   FOR ctr2 = 0 TO (noscripts - 1)
    IF scriptname$(ctr2) = realname$ THEN
     operatordata(ctr, operatorkind) = 7
     operatordata(ctr, operatorid) = ctr2
     foundequal = 1
     EXIT FOR
    END IF
   NEXT
  END IF
  IF foundequal = 0 THEN
   FOR ctr2 = 0 TO UBOUND(hardfunction$) 'Though I don't know why anyone would make an operator out of a function, heres some support for it
    IF hardfunction$(ctr2) = realname$ THEN
     operatordata(ctr, operatorkind) = 6
     operatordata(ctr, operatorid) = ctr2
     foundequal = 1
     EXIT FOR
    END IF
   NEXT
  END IF
  IF foundequal = 0 THEN  'Couldn't find a func, mathf, or script with the same name
   errordeal("Operator refers to function or script which does exist or isn't defined. Name was", realname$)
  END IF
 NEXT
 'Process same name operators here
 'Change: Nevermind, I not needed.
 'REDIM tempopreal$(0)  'shrink down the operator name to nill, not needed anymore
 ERASE tempopreal$
 IF nofunctions THEN
  REDIM PRESERVE hardfunction$(highestfunc)
 END IF
 IF nooperators THEN
  REDIM PRESERVE operatordata(nooperators - 1, 3)
  REDIM PRESERVE operatorname$(nooperators - 1)
 END IF
END SUB

SUB readcommandstr
 COLOR 2: PRINT "  Hamsterspeak Decompiler  v1.13   TMC - 2003, 2011, 2017, 2020": COLOR 7
 PRINT
 ' argnum = 1
 ' WHILE COMMAND(argnum) <> ""
 '  PRINT "arg " & argnum & ": '" & COMMAND(argnum) & "'"
 '  argnum += 1
 ' WEND
 arg$ = COMMAND(1)
 IF arg$ = "/?" OR arg$ = "/h" OR arg$ = "--help" OR arg$ = "-h" THEN
  PRINT "without non-debug switchs to enter user interface."
  PRINT "List of command-line options:"
  PRINT
  PRINT "  /? or /H      - Command-line help, what you are viewing now"
  'PRINT "  /P            - Print out output on screen instead of saving to file "
  PRINT "  /F:<name>     - Load formatting options by name (no file extension)"
  PRINT "  /Di           - Turns on Debug Mode for included files"
  PRINT "  /Dh           - Turns on Debug Mode for the HS file"
  PRINT "  /Dm           - Turns on Debug Mode for compared scripts"
  PRINT
  PRINT "Syntax:  HSDECMPL [<hs(p) file> [<hss output file>] [<include files>] [<optionlist>] ]"
  PRINT "Leave a double space to skip a filename. Separate include files with +'s"
  PRINT "EG."
  PRINT "  HSDECMPL myrpg.hs myrpg.hss plotscr.hsd+scancode.hsi+myrpg.hsi /F:default"
  PRINT "  HSDECMPL /path/to/myrpg.hs  plotscr.hsd"
  PRINT "  HSDECMPL myrpg.hs   /F:default"
  CLOSE
  END
 END IF

 hsfile$ = COMMAND(1)
 IF COMMAND(2) = "" THEN 'ok, this ay be windows drag drop
/'
  readstr$ = COMMAND(1)
  IF INSTR(readstr$, "\") THEN
   anastring$ = ""
   WHILE INSTR(readstr$, "\")   'this cuts the path name out of it.
    anastring$ = anastring$ + MID$(readstr$, 1, INSTR(readstr$, "\"))
    readstr$ = MID$(readstr$, INSTR(readstr$, "\") + 1)
   WEND
   anastring$ = MID$(anastring$, 1, LEN(anastring$) - 1) 'cut the last \ off
   CHDIR anastring$
 '  PRINT "now: -"; readstr$; "-"
 '  PRINT "current dir is "; anastring$
  END IF
  hsfile$ = readstr$
'/
  EXIT SUB
 END IF
 hssfile$ = COMMAND(2)
 IF COMMAND(3) = "" THEN EXIT SUB
 readstr$ = COMMAND(3)
 IF readstr$ <> " " THEN
  DO
   IF INSTR(readstr$, "+") THEN
    includenames$(noincludes) = MID$(readstr$, 1, INSTR(readstr$, "+") - 1)
    readstr$ = MID$(readstr$, INSTR(readstr$, "+") + 1)
   ELSE
    includenames$(noincludes) = readstr$
    readstr$ = ""
   END IF
   noincludes = noincludes + 1
  LOOP WHILE readstr$ <> ""  'INSTR(readstr$, "+")
 END IF
 IF COMMAND(4) = "" THEN EXIT SUB
 'now for optionlist
 argnum = 4
 WHILE COMMAND(argnum) <> ""
  readstr$ = LCASE(COMMAND(argnum))
  argnum += 1
  IF LEFT(readstr$, 1) = "/" THEN
   readstr$ = MID(readstr$, 2)
   IF INSTR(readstr$, ":") THEN
    SWstr$ = MID$(readstr$, INSTR(readstr$, ":") + 1)
    SWvalue = VAL(SWstr$)
    readstr$ = MID$(readstr$, 1, INSTR(readstr$, ":") - 1)
   END IF
   SELECT CASE LCASE(readstr$)
    CASE "di"
     setstyleoption(HSIdebugon, 1)
    CASE "dh"
     setstyleoption(HSdebugon, 1)
    CASE "dm"
     setstyleoption(MSdebugon, 1)
    CASE "spiffy", "bob", "hamster"
     PRINT "  Spiffyness!"
    CASE "f"
     setstyleoption(noformatneeded, 1)
     readformatfile(SWstr$)
    CASE ELSE
     errordeal("Command-line error: unknown switch", "/" + readstr$)
   END SELECT
  END IF
 WEND
END SUB

SUB readformatfile (fname$)
 DIM as SHORT verno
 IF LEN(fname$) > 8 THEN errordeal("Formatting options filename too long. Name was ", fname$)
 fname$ = LCASE$(fname$)
 IF fname$ = "theworks" THEN
  foundit = 1
  stylevars(spacesindent) = 2
  stylevars(baseindent) = 1
  stylevars(defineindent) = 1
  styleoptions(0) = 2147483646  'here
  styleoptions(1) = styleoptions(1) OR 510 'here
 END IF
 IF fname$ = "default" THEN
  foundit = 1
  stylevars(spacesindent) = 2
  stylevars(baseindent) = 2
  stylevars(defineindent) = 2
  styleoptions(0) = 1083204690
  styleoptions(1) = styleoptions(1) OR 55 'here
 END IF
 IF fname$ = "C" THEN
  foundit = 1
  stylevars(spacesindent) = 4
  stylevars(baseindent) = 2
  stylevars(defineindent) = 1
  styleoptions(0) = 1252020280  'here
  styleoptions(1) = styleoptions(1) OR 3 'here
 END IF
 IF fname$ = "clearcut" THEN
  foundit = 1
  stylevars(spacesindent) = 2
  stylevars(baseindent) = 2
  stylevars(defineindent) = 1
  styleoptions(0) = 1137716344  'here
  styleoptions(1) = styleoptions(1) OR 51 'here
 END IF
' PRINT "found it is"; foundit
 IF foundit = 0 THEN
  fname$ = fname$ + ".dcf"
  filesno = FREEFILE
  PRINT "Reading formatting options from "; fname$
  OPEN fname$ FOR BINARY AS filesno
'  PRINT "Length file is"; LOF(filesno)
  IF LOF(filesno) = 0 THEN
   CLOSE filesno
   errordeal("Formatting options file does not exist. Filename was ", fname$)
  END IF
  headerm$ = "        "
  GET #filesno, , headerm$
  GET #filesno, , verno
'  PRINT "version id is"; verno
  IF LOF(filesno) <> 38 OR headerm$ <> "HSDECMPL" OR verno <> 1001 THEN
   CLOSE filesno
   errordeal("Formatting options file corrupted or not a dcf file. Filename was ", fname$)
  END IF
  GET #filesno, , styleoptions(0)
  GET #filesno, , temp
  styleoptions(1) = styleoptions(1) OR temp
  FOR i = 0 TO 9
   GET #filesno, , stylevars(i)
  NEXT
  CLOSE filesno
 END IF
END SUB

SUB readHS
 DIM as SHORT temp1
 PRINT "Reading HS file "; hsfile$
 fh = FREEFILE
 OPEN hsfile$ FOR BINARY AS #fh
' PRINT hsfile$
 currentfile = fh
 lumpfile = 0
 flen = LOF(fh)
 IF flen < 2 THEN
  CLOSE #fh
  KILL hsfile$
  errordeal("HS file does not exist. Filename was ", hsfile$)
 END IF
 check$ = "  "
 GET #currentfile, , check$
 IF check$ <> "HS" THEN
  errordeal("Not a HS file. Incorrect header.", "")
 END IF
 GET #fh, 20, temp1
 verno = temp1
 IF verno > 3 OR verno < 0 THEN errordeal("HS file is of an unsupported version. Version found was", verno)
 IF verno < 2 THEN COLOR 4: PRINT "Warning:- HS file is of an old version. May not decompile properly": COLOR 7
 SEEK #fh, 1

 REDIM PRESERVE lumpnames$(0 TO 50), lumppointerindex(0 TO 50)
 currentlydim = 50

 fname$ = ""
 inchar$ = " "
 PRINT "Length of file:"; flen
 keeploop = 1
 nolumps = 0
 WHILE keeploop = 1
  fname$ = ""
  'PRINT "Current position:"; SEEK(fh)
  GET #fh, , inchar$
  WHILE ASC(inchar$) <> 0
   fname$ = fname$ + inchar$
   IF LEN(fname$) > 20 THEN
    CLOSE #fh
    errordeal("Corrupted HS file: lumped filename too long. Name was ", fname$)
   END IF
   GET #fh, , inchar$
  WEND
  'PRINT "(read name) Lumped filename: "; fname$
  datalen = 0
  GET #fh, , temp1
  datalen = temp1 SHL 16
  GET #fh, , temp1
  datalen += temp1

  lumppointerindex(nolumps) = SEEK(fh)  'the next byte is the start of data
  lumpnames$(nolumps) = UCASE$(fname$)

  debug "read lump " & nolumps & ": " & lumpnames$(nolumps) & " at " & lumppointerindex(nolumps) & " len " & datalen
  nolumps = nolumps + 1

  IF nolumps >= currentlydim THEN
   currentlydim = currentlydim + 10
   REDIM PRESERVE lumpnames$(0 TO currentlydim), lumppointerindex(0 TO currentlydim)
  END IF
'  PRINT "(read datalen) datalen of file is"; datalen; "(new position)"; SEEK(fh)
  temp = SEEK(fh) + datalen
'  PRINT "Temp ="; temp; "flen ="; flen
  IF temp < flen THEN
   SEEK #fh, SEEK(fh) + datalen  'As long as the next file is not the last (in which case stop)
  ELSE
   keeploop = 0
  END IF
  'PRINT "KL ="; keeploop
 WEND

 REDIM PRESERVE lumpnames$(0 TO nolumps - 1), lumppointerindex(0 TO nolumps - 1)

 scripts_txt = -1
 FOR i = 0 TO nolumps - 1
  IF lumpnames$(i) = "SCRIPTS.TXT" THEN scripts_txt = i : EXIT FOR
 NEXT
 IF scripts_txt = -1 THEN
  CLOSE
  errordeal("The scripts index SCRIPTS.TXT could not be found in the HS file.", "")
 END IF

 inchar$ = " "
 state = 0
 noscripts = 0
 REDIM PRESERVE scriptname$(0 TO 50), scriptdata(0 TO 50, 0 TO 1)
 currentlydim = 50
 SEEK #fh, lumppointerindex(scripts_txt)

 WHILE SEEK(fh) <= lumppointerindex(scripts_txt + 1)
  GET #fh, , inchar$
  IF inchar$ = CHR$(13) THEN
 '  PRINT "Got state:"; state
   SELECT CASE state
   CASE 0 'get script name
    scriptname$(noscripts) = curstring$
    state = 1
   CASE 1 'script id
    scriptdata(noscripts, scriptid) = VAL(curstring$)
    state = 2
   CASE 2 'no of args
    noofargs = VAL(curstring$)
    curarg = 0
    scriptdata(noscripts, scriptargs) = noofargs
    noscripts = noscripts + 1
    IF noscripts >= currentlydim THEN
     currentlydim = currentlydim + 10
     REDIM PRESERVE scriptname$(0 TO currentlydim), scriptdata(0 TO currentlydim, 0 TO 1)
    END IF
    IF noofargs > 0 THEN
     state = 3
    ELSE
     state = 0
    END IF
   CASE 3 'read args
    curarg = curarg + 1
    IF curarg = noofargs THEN state = 0
   END SELECT
   curstring$ = ""
   GET #fh, , inchar$  'go past the next character, which is no 10
  ELSE
   curstring$ = curstring$ + inchar$
  END IF
 WEND
 REDIM PRESERVE scriptname$(0 TO noscripts - 1), scriptdata(0 TO noscripts - 1, 0 TO 1)

 setstring = -1
 appendstring = -1
 FOR ctr = 0 TO (noscripts - 1)
  IF scriptname$(ctr) = "setstring" THEN setstring = scriptdata(ctr, scriptid)
  IF scriptname$(ctr) = "appendstring" THEN appendstring = scriptdata(ctr, scriptid)
 NEXT
 debug "setstring = " & setstring
 debug "appendstring = " & appendstring
' debug "32711 = " & scriptname$(returnscriptdata(arrayno, 32711))
' debug "32712 = " & scriptname$(returnscriptdata(arrayno, 32712))
END SUB

SUB readHSdebug
 COLOR 1: PRINT "HS file debug - ": COLOR 7
 PRINT "No lumped files in HS: " & nolumps
 FOR ctr = 0 TO nolumps - 1
  PRINT "Pointer of lumped file " & ctr & " is " & lumppointerindex(ctr)
 NEXT
 PRINT "No scripts read from HS file: " & noscript
 FOR ctr = 0 TO noscripts - 1
  PRINT "Script no " & ctr & " has name " & scriptname$(ctr) & " ID is " & scriptdata(ctr, scriptid) & " number of args: " & scriptdata(ctr, scriptargs)
 NEXT
 PRINT
END SUB

SUB readinclude   'Note to self: Everything is written now
 filepo = 1
' WHILE INKEY$ = "" : WEND
 cmdstring$ = ""
 enter1$ = CHR$(13)  'NOTE: THESE 2 MUST BE IN THE RIGHT ORDER!!
 enter2$ = CHR$(10)   'Heres a sneaky thing: I only check for either of the 2 enter charcters
 inchar$ = " "
 lastchar$ = ""
 linenum = 1
 readstate = nostate
 WHILE filepo <= LOF(includefile)
  GET #includefile, , inchar$
'  PRINT inchar$; " ";
  IF inchar$ = enter1$ OR (inchar$ = enter2$ AND lastchar$ <> enter1$) THEN linenum += 1
  lastchar$ = inchar$
  IF (inchar$ = "," AND commentstate = 0) OR inchar$ = enter1$ OR inchar$ = enter2$ THEN
   'process the cmd
   IF styleoption(usefoundname) THEN foundtype$ = TRIM$(cmdstring$)
   cmdstring$ = normalizestring$(cmdstring$)
'   debug cmdstring$
   IF inchar$ = enter1$ OR inchar$ = enter2$ THEN commentstate = 0 'Comments end at a new line
   IF LEN(cmdstring$) > 0 THEN    'check that its not just a blank line or return
    'check for any builtin operators that don't need commas

    splitline cmdstring$, foundtype$, 0

    cmdstring$ = ""
   END IF
  ELSE
   IF inchar$ = "#" THEN commentstate = commentedout
   IF commentstate <> commentedout AND ASC(inchar$) >= 32 THEN cmdstring$ = cmdstring$ + inchar$
  END IF
  filepo = filepo + 1
 WEND
 debug "filepo = " & filepo & " LOF = " & LOF(includefile)
END SUB

SUB splitline (cmdstring$, rawstring$, searchstart)
'rawstring$ is a segment of text from an include file without spaces (already split by spaces)
'cmdstring$ is that line with spaces removed
'searchstart is the number of the operator to start searching for

 FOR opno = searchstart TO UBOUND(builtinop$)
  temp = INSTR(cmdstring$, builtinop$(opno))

  IF temp THEN
   IF temp <> 1 THEN
    'continue to scan the text before the operator for more operators
    readstring$ = MID$(cmdstring$, 1, temp - 1)
    GOSUB returnftypeop
    splitline readstring$, foundtype$, opno + 1
   END IF

   'send the operator to processcmd (do NOT attempt to split it)
   readstring$ = builtinop$(opno)
   GOSUB returnftypeop
   processcmd readstring$, foundtype$
   
   IF temp - 1 + LEN(builtinop$(opno)) < LEN(cmdstring$) THEN
    'check everything after the operator for splitting
    readstring$ = MID$(cmdstring$, temp + LEN(builtinop$(opno)))
    GOSUB returnftypeop
    splitline readstring$, foundtype$, opno
   END IF

   EXIT SUB
  END IF
 NEXT

 'no ops found, send as a single string
 readstring$ = cmdstring$
 GOSUB returnftypeop
 processcmd readstring$, foundtype$
 EXIT SUB

returnftypeop:
 chartobite = 0
 ctrui = 1
 WHILE chartobite < LEN(readstring$)
  IF MID$(rawstring$, ctrui, 1) <> " " THEN chartobite += 1
  ctrui += 1
 WEND
 foundtype$ = TRIM$(MID$(rawstring$, 1, ctrui - 1))  'break off a chunk for foundtype
 rawstring$ = MID$(rawstring$, ctrui) 'shorten the source
'debug "readstring :: '" & readstring$ & "' '" & foundtype$ & "'"
RETURN

END SUB


SUB processcmd (cmdstring$, foundtype$)
STATIC currentreadarg, currentid, noofargs

'debug "process: str = '" + cmdstring$ + "' raw = '" + foundtype$ + "'"
'EXIT SUB

     'debug "dowork, cmdstring$ = " & cmdstring$ + " st = " & readstate
     IF cmdstring$ = "(" OR cmdstring$ = "begin" THEN isbegin = -1 ELSE isbegin = 0
     IF cmdstring$ = "end" OR cmdstring$ = ")" THEN isend = -1 ELSE isend = 0
     IF cmdstring$ = "autonumber" THEN cmdstring$ = "-1"
     IF cmdstring$ = "none" THEN cmdstring$ = "0"   'the only constants accepted
   '   PRINT "state:"; readstate; "cmd: "; cmdstring$; " foundtype: "; foundtype$
     SELECT CASE readstate
      'nostate,
      CASE readfuncname, readfuncargcount, readfuncargs, readoperatorname, readoperatorreal, readscriptname, readscriptsnargs, readscriptargs, readglobalname, readconstantname
       IF isbegin OR isend THEN
        errordeal("Incorrectly formatted include file on line " & linenum & ". Expected a symbol or value but found ", cmdstring$)
       END IF
      CASE readfunctionid, readoperatorval, readscriptid, readglobalid, readconstantid
       IF isbegin THEN
        errordeal("Incorrectly formatted include file on line " & linenum & ". Expected a symbol or value or end but found ", cmdstring$)
       END IF
      CASE opendfunction, opendoperator, opendscript, opendglobal, opendconstant
       IF NOT isbegin THEN 
        errordeal("Incorrectly formatted include file on line " & linenum & ". Expected '(' or 'begin' but found ", cmdstring$)
       END IF
     END SELECT

     SELECT CASE readstate
      CASE nostate
       IF cmdstring$ = "definefunction" THEN readstate = opendfunction
       IF cmdstring$ = "defineoperator" THEN readstate = opendoperator
       IF cmdstring$ = "definescript" THEN readstate = opendscript
       IF cmdstring$ = "globalvariable" THEN readstate = opendglobal
       IF cmdstring$ = "defineconstant" THEN readstate = opendconstant
       IF cmdstring$ = "script" OR cmdstring$ = "plotscript" THEN readstate = readscriptblockname
       IF cmdstring$ = "definetrigger" THEN readstate = opendtrigger
      CASE opendtrigger
       IF isend THEN
        readstate = nostate
       END IF

      'define function block
      CASE opendfunction
       readstate = readfunctionid
      CASE readfunctionid
       IF isend THEN
        readstate = nostate
       ELSE
        currentid = VAL(cmdstring$)
        readstate = readfuncname
        IF currentid > UBOUND(hardfunction$) THEN 'doesn't fit in normal function array
         highfunctionid(nohighfuncs) = currentid
         currentid = -1
        ELSE
         IF currentid > highestfunc THEN highestfunc = currentid
        END IF
        nofunctions = nofunctions + 1   'just for the record
       END IF
      CASE readfuncname
       IF currentid = -1 THEN
        IF styleoption(usefoundname) THEN
         highfunction$(nohighfuncs) = foundtype$
        ELSE
         highfunction$(nohighfuncs) = cmdstring$
        END IF
        nohighfuncs = nohighfuncs + 1
       ELSE
        IF styleoption(usefoundname) THEN
         hardfunction$(currentid) = foundtype$
        ELSE
         hardfunction$(currentid) = cmdstring$
        END IF
       END IF
       readstate = readfuncargcount
      CASE readfuncargcount
       noofargs = VAL(cmdstring$)
       IF noofargs > 0 THEN
        readstate = readfuncargs
        currentreadarg = 0
       ELSE
        readstate = readfunctionid
       END IF
      CASE readfuncargs         'Throw those default args away! Decompiling has no use for them :P
       currentreadarg = currentreadarg + 1
       IF currentreadarg = noofargs THEN readstate = readfunctionid
      CASE opendoperator
       readstate = readoperatorval
      CASE readoperatorval
       IF isend THEN
        readstate = nostate
       ELSE
        operatordata(nooperators, operatorval) = VAL(cmdstring$)
        readstate = readoperatorname
       END IF
      CASE readoperatorname
       operatorname$(nooperators) = cmdstring$    'define operator doesn't use foundtype
       readstate = readoperatorreal
       tempnamecheck$ = cmdstring$
      CASE readoperatorreal
       tempopreal$(nooperators) = cmdstring$
       readstate = readoperatorval
       IF cmdstring$ = tempnamecheck$ THEN 'If the real and shown names are the same, the op must be used
        operatordata(nooperators, opmustbeused) = 1
       ELSE
        operatordata(nooperators, opmustbeused) = 0
       END IF
       nooperators = nooperators + 1

      'script/plotscript blocks
      CASE readscriptblockname
       readstate = nostate
       'check whether already definescript'd
       FOR i = 0 TO includedscripts - 1
        IF normalstring(includedscriptname$(i)) = cmdstring$ THEN
         debug "read script block: " & cmdstring$ & " already defined."
         GOTO cancel
        END IF
       NEXT
       'redim for the bigger the included scripts arrays
       REDIM PRESERVE includedscriptid(includedscripts + 1), includedscriptname$(includedscripts + 1)
       IF styleoption(usefoundname) THEN
        includedscriptname$(includedscripts) = foundtype$
       ELSE
        includedscriptname$(includedscripts) = cmdstring$
       END IF
       includedscriptid(includedscripts) = -1
       debug "read script block: id -1, name = " & includedscriptname$(includedscripts)
       includedscripts = includedscripts + 1
       cancel:

      'definescript blocks
      CASE opendscript
       readstate = readscriptid
      CASE readscriptid
       IF isend THEN    'all scritps stuff needs finishing/checking
        readstate = nostate
       ELSE
        readstate = readscriptname
       END IF
       includedscriptid(includedscripts) = VAL(cmdstring$)
       IF cmdstring$ = "autonumber" THEN includedscriptid(includedscripts) = -1
debug "cmdstr = " & cmdstring$
      CASE readscriptname
       readstate = readscriptsnargs
       'redim for the bigger the included scripts arrays
       REDIM PRESERVE includedscriptid(includedscripts + 1), includedscriptname$(includedscripts + 1)
       IF styleoption(usefoundname) THEN
        includedscriptname$(includedscripts) = foundtype$
       ELSE
        includedscriptname$(includedscripts) = cmdstring$
       END IF
debug "read script: id = " & includedscriptid(includedscripts) & " name = " & includedscriptname$(includedscripts)
       includedscripts = includedscripts + 1
      CASE readscriptsnargs
       noofargs = VAL(cmdstring$)
       IF noofargs > 0 THEN
        currentreadarg = 0
        readstate = readscriptargs
       ELSE
        readstate = readscriptid
       END IF
      CASE readscriptargs
       currentreadarg = currentreadarg + 1
       IF currentreadarg = noofargs THEN readstate = readscriptid

      'global variable block
      CASE opendglobal
       readstate = readglobalid
      CASE readglobalid
       IF isend THEN
        readstate = nostate
       ELSE
        currentid = VAL(cmdstring$)
        readstate = readglobalname
       END IF
      CASE readglobalname
       IF styleoption(usefoundname) THEN
        globalname$(currentid) = foundtype$
        PRINT "naming global no "; currentid; foundtype$
       ELSE
        globalname$(currentid) = cmdstring$
        PRINT "naming global no "; currentid; cmdstring$
       END IF
       readstate = readglobalid
       nofoundglobals = nofoundglobals + 1

      'define constant block
      CASE opendconstant
       readstate = readconstantid
      CASE readconstantid
       IF isend THEN
        readstate = nostate
       ELSE
        currentid = VAL(cmdstring$)
        readstate = readconstantname
       END IF
      CASE readconstantname
       addconstant(cmdstring$, foundtype$, currentid)
'       FOR i = 0 TO UBOUND(ruleconstants)
'        WITH ruleconstants(i)
'         IF MID$(cmdstring$, 1, LEN(.str)) = .str THEN
'          IF styleoption(usefoundname) THEN
'           addconstant(.num, foundtype$, currentid)
'          ELSE
'           addconstant(.num, cmdstring$, currentid)
'          END IF
'          EXIT FOR
'         END IF
'        END WITH
'       NEXT
       readstate = readconstantid
     END SELECT

END SUB

FUNCTION returnglobalname$ (globalno)
 IF LEN(globalname$(globalno)) THEN
  returnglobalname$ = globalname$(globalno)
 ELSE
  FOR ctr = 0 TO noaddglobals - 1
   IF globalsdec(ctr) = globalno THEN      'Check if the globals already been added
    alreadyadded = 1
    EXIT FOR
   END IF
  NEXT
'  PRINT "Global id is"; globalno; "already added is"; alreadyadded
  returnglobalname$ = "global" + LTRIM$(STR$(globalno))
  IF alreadyadded = 0 THEN
   globalsdec(noaddglobals) = globalno
   noaddglobals = noaddglobals + 1
   IF noaddglobals MOD 20 = 0 THEN REDIM PRESERVE globalsdec(0 TO noaddglobals + 19)
  END IF
 END IF
END FUNCTION

FUNCTION returnlocalname$ (localno)
 IF styleoption(nameargsseparate) AND localno < thisscr.args THEN basename$ = "arg" ELSE basename$ = "local"
 IF styleoption(nameargsseparate) AND localno >= thisscr.args THEN varnumber = localno - thisscr.args ELSE varnumber = localno
 IF styleoption(namevarsfrom1) THEN varnumber = varnumber + 1
 returnlocalname$ = basename$ + LTRIM$(STR$(varnumber))

' IF styleoption(nameargsseparate) THEN
'  IF localno < thisscr.args THEN 'is an arg and has its own numbering scheme
'   IF styleoption(namevarsfrom1) THEN
'    returnlocalname$ = "arg" + LTRIM$(STR$(localno + 1))
'   ELSE
'    returnlocalname$ = "arg" + LTRIM$(STR$(localno))
'   END IF
'  ELSE
'   IF styleoption(namevarsfrom1) THEN
'    returnlocalname$ = "local" + LTRIM$(STR$(localno - thisscr.args + 1))
'   ELSE
'    returnlocalname$ = "local" + LTRIM$(STR$(localno - thisscr.args))
'   END IF
'  END IF
' ELSE
'  IF styleoption(namevarsfrom1) THEN
'   returnlocalname$ = "local" + LTRIM$(STR$(localno + 1))
'  ELSE
'   returnlocalname$ = "local" + LTRIM$(STR$(localno))
'  END IF
' END IF
END FUNCTION

FUNCTION returnscriptdata (jobtype, id)
 dataid = -1
 FOR ctr = 0 TO (noscripts - 1)
  IF scriptdata(ctr, scriptdataid) = id THEN
   dataid = ctr
   EXIT FOR
  END IF
 NEXT
 IF dataid = -1 THEN errordeal("Script could not be found by id number in script data array. (Possibly compiler bug) ID was", id)
' IF jobtype = scriptname THEN returnscriptdata = scriptname$(dataid)
 IF jobtype = arrayno THEN returnscriptdata = dataid
 IF jobtype = scriptargs THEN returnscriptdata = scriptdata(dataid, scriptdataargs)
' IF jobtype = deafaultarg THEN returnscriptdata = argdefault(dataid, argno) 'Ummmm?
END FUNCTION

SUB setstyleoption (opno, value)
' PRINT opno
 temp = styleoptions(opno \ 31) AND 2 ^ (opno MOD 31)
 IF temp THEN temp = 1
 'PRINT "opno is"; opno ;"value is"; value; "sum is"; styleoptions(opno \ 31)
 IF temp <> value THEN
  IF value THEN
   styleoptions(opno \ 31) = styleoptions(opno \ 31) + 2 ^ (opno MOD 31)
  ELSE
   styleoptions(opno \ 31) = styleoptions(opno \ 31) - 2 ^ (opno MOD 31)
  END IF
 END IF
END SUB

SUB simplesortglobals
 IF noaddglobals > 1 THEN
  PRINT "Sorting global variables"
  FOR ctri = 0 TO noaddglobals - 2
   FOR ctrj = ctri + 1 TO noaddglobals - 1
    IF globalsdec(ctri) > globalsdec(ctrj) THEN
     temp = globalsdec(ctri)
     globalsdec(ctri) = globalsdec(ctrj)
     globalsdec(ctrj) = temp
    END IF
   NEXT
  NEXT
 ELSE
  PRINT "Skipping global variable sorting"
 END IF
END SUB

FUNCTION styleoption (opno)
 temp = styleoptions(opno \ 31) AND 2 ^ (opno MOD 31)
 IF temp THEN styleoption = -1
END FUNCTION

SUB timelesswait
 temp = TIMER * 1000
 WHILE INKEY$ = "": WEND
 starttime = starttime + (TIMER * 1000 - temp)
END SUB

SUB writeformatfile (fname$)
 DIM as SHORT verno
 IF LEN(fname$) > 8 THEN errordeal("Formatting options filename too long. Name was ", fname$)
 fname$ = fname$ + ".dcf"
 fh = FREEFILE
 OPEN fname$ FOR BINARY AS #fh
 IF LOF(fh) > 0 THEN
  PRINT "Format options file ";
  COLOR 15: PRINT fname$; : COLOR 7: PRINT " already exists. Overwrite? (y/n)"
  PRINT "    ";
  answer$ = INKEY$   'This is timeleswait modified
  temp = TIMER * 1000
  WHILE answer$ = "": answer$ = INKEY$: WEND
  starttime = starttime + (TIMER * 1000 - temp)
  IF LCASE$(answer$) = "y" THEN
   COLOR 14: PRINT "y": COLOR 7
   CLOSE #fh
   KILL fname$
   OPEN fname$ FOR BINARY AS #fh
  ELSE
   PRINT "Bye then"
   CLOSE
   END -1
  END IF
 END IF
 outputp$ = "HSDECMPL"
 verno = 1001
 PUT #fh, , outputp$
 PUT #fh, , verno
 PUT #fh, , styleoptions(0)
 temp = (styleoptions(1) MOD 1024)
 PUT #fh, , temp  'just the first 9 options
 FOR i = 0 TO 9
  PUT #fh, , stylevars(i)
 NEXT
 'PRINT "sig 1 is"; styleoptions(0)
 'PRINT "sig 2 is"; (styleoptions(1) MOD 1024)
END SUB

SUB writescriptender
   IF styleoption(usebeginendtoplevel) THEN
    outputcmd$ = "end" + newline$
   ELSE
    outputcmd$ = ")" + newline$
   END IF
   PUT #tempfile, , outputcmd$

END SUB

SUB writescriptheader (noargs, novars, scriptnumber)
   outputcmd$ = newline$
   IF styleoption(linebetweenscripts) THEN
    outputcmd$ = outputcmd$ + "#" + STRING$(50, "-") + newline$ + newline$
   END IF
   outputcmd$ = outputcmd$ + "script, " + scriptname$(scriptnumber)

'   PRINT "Now for args. No vars:"; novars;" Noargs:"; noargs
   'write args
   FOR p = 1 TO noargs
    IF styleoption(nameargsseparate) THEN
     outputcmd$ = outputcmd$ + ", arg"
    ELSE
     outputcmd$ = outputcmd$ + ", local"
    END IF
    IF styleoption(namevarsfrom1) THEN
     outputcmd$ = outputcmd$ + LTRIM$(STR$(p))
    ELSE
     outputcmd$ = outputcmd$ + LTRIM$(STR$(p - 1))
    END IF
   NEXT

   IF styleoption(flowbeginnewline) THEN outputcmd$ = outputcmd$ + newline$
   IF styleoption(usebeginendtoplevel) THEN
    IF styleoption(flowbeginnewline) THEN
     outputcmd$ = outputcmd$ + "begin"
    ELSE
     outputcmd$ = outputcmd$ + ", begin"
    END IF
    outputcmd$ = outputcmd$ + newline$
   ELSE
    outputcmd$ = outputcmd$ + " (" + newline$
   END IF
 '  PRINT "now for vars."

   'write variables
   FOR p = 1 TO (novars - noargs)
    IF styleoption(separatevariables) OR p = 1 THEN
 '    PRINT "first go. syltop ="; styleoption(dontindentvars)
     IF styleoption(dontindentvars) = 0 THEN outputcmd$ = outputcmd$ + SPACE$(stylevars(baseindent))
     outputcmd$ = outputcmd$ + "variable (local"
    ELSE
     outputcmd$ = outputcmd$ + ", local"
    END IF
    IF styleoption(nameargsseparate) THEN
     IF styleoption(namevarsfrom1) THEN
      outputcmd$ = outputcmd$ + LTRIM$(STR$(p))
     ELSE
      outputcmd$ = outputcmd$ + LTRIM$(STR$(p - 1))
     END IF
    ELSE
     IF styleoption(namevarsfrom1) THEN
      outputcmd$ = outputcmd$ + LTRIM$(STR$(noargs + p))
     ELSE
      outputcmd$ = outputcmd$ + LTRIM$(STR$(noargs + p - 1))
     END IF
    END IF
    IF styleoption(separatevariables) OR p = (novars - noargs) THEN outputcmd$ = outputcmd$ + ")" + newline$
   NEXT
   IF styleoption(lineaftervariables) AND (novars - noargs) > 0 THEN outputcmd$ = outputcmd$ + newline$
   PUT #tempfile, , outputcmd$

END SUB

SUB debug (dbstr$)
fh = FREEFILE
OPEN "hsdecmpl_debug.txt" FOR APPEND AS #fh
PUT #fh, , dbstr$
PUT #fh, , newline$
CLOSE #fh
END SUB

FUNCTION processoldstring$ (cmdid, wordref)
'The old set/appendstring scripts.
'Took ascii values as arguments. appendstring was also called from in setstring, and in
'that case script args were passed.
'This only works if all the parameters are constants.

' argcount = getword(wordref + 2)  41

 FOR ctr = 2 TO 41
  'IF not a constant, quit
  IF getword(getword(wordref + ctr + 2) + 0) <> 1 THEN EXIT FUNCTION
 NEXT

 temp$ = "$" + processcommand$(getword(wordref + 3))
 IF cmdid = setstring THEN temp$ += " =" + CHR$(34)
 IF cmdid = appendstring THEN temp$ += " +" + CHR$(34)
 FOR ctr = 2 TO 41
  IF getword(getword(wordref + ctr + 2) + 1) = -1 THEN EXIT FOR
  IF getword(getword(wordref + ctr + 2) + 1) = 34 OR getword(wordref + ctr + 2) = 92 THEN temp$ = temp$ + "\"
  temp$ = temp$ + CHR$(getword(getword(wordref + ctr + 2) + 1))
 NEXT
 processoldstring$ = temp$ + CHR$(34)

END FUNCTION

FUNCTION read32bitstring$(offset)
 DIM temp as integer
 GET #currentfile, offset, temp
 if temp > 200 then read32bitstring$ = "<<TOO LONG>>": exit function
 DIM as string result = SPACE(temp)
 GET #currentfile, offset + 4, result
 debug "32bitstring: read len " & temp & " :'" & result & "'"
 read32bitstring$ = result
END FUNCTION

FUNCTION readstringtable$(offset)
 IF thisscr.strtable = 0 THEN errordeal("Script has no string header yet refers to it!", "")
 readstringtable$ = read32bitstring(thisscr.strtable + offset * 4)
END FUNCTION

FUNCTION processnewstring$ (cmdid, wordref)
'The new set/appendstringfromtable commands

 temp$ = "$" + processcommand$(getword(wordref + 3))
 IF cmdid = 251 THEN temp$ += " = """
 IF cmdid = 252 THEN temp$ += " + """
 
 s$ = readstringtable$(getword(getword(wordref + 4) + 1))

 FOR ctr = 1 TO LEN(s$)
  IF INSTR("""\", MID(s$, ctr, 1)) THEN temp$ += "\"
  temp$ += MID(s$, ctr, 1)
 NEXT
 processnewstring$ = temp$ + CHR$(34)

END FUNCTION


FUNCTION getconstantfromrule$ (startrule, value)
 copy = value
 rule = startrule
 IF rule <> -1 THEN
  temp$ = ""
  IF rule >= 1000000 THEN bitmask = -1 : rule -= 1000000
  WITH constantlookup(rule)
   FOR i = 0 TO .num - 1
    IF .table[i].num = value THEN
     getconstantfromrule$ = temp$ + .table[i].str
     EXIT FUNCTION
    END IF
    IF bitmask THEN
     debug "bitmask!"
     IF (value AND .table[i].num) = .table[i].num THEN
      temp$ = temp$ & .table[i].str & " + "
      value -= .table[i].num
     END IF
    END IF
   NEXT
  END WITH
 END IF
 getconstantfromrule$ = STR$(copy)

END FUNCTION

SUB addconstanttorule (rule, foundtype$, value)
 debug "ubound = " & UBOUND(constantlookup)
 debug "rule " & rule & "+=cons: str = '" & foundtype$ & "' val = " & value
 WITH constantlookup(rule)
  debug "cons num = " & .num & " size = " & .size
  IF .size = 0 THEN
   .num = 0
   .size = 200
   .table = callocate(.size, LEN(StrNum))
   IF .table = 0 THEN PRINT "COULDN'T ALLOCATE MEM" : END
  END IF
  IF .size <= .num THEN
   'print .table & " " & .size & " " & LEN(StrNum)
   .size *= 2
   .table = reallocate(.table, .size * LEN(StrNum))
   'print .table & " " & .size & " " & LEN(StrNum)
   IF .table = 0 THEN PRINT "COULDN'T ALLOCATE MEM2" : END
   FOR i = .size / 2 TO .size - 1
    .table[i].str = ""
   NEXT
  END IF
  .table[.num].str = foundtype$
  .table[.num].num = value
  .num += 1
 END WITH
END SUB

SUB addconstant (cmdstring$, found$, value)
 debug "adding cons: str = '" & cmdstring$ & "' real = '" & found$ & "' val = " & value
 constants(UBOUND(constants)).str = cmdstring$
 constants(UBOUND(constants)).real = found$
 constants(UBOUND(constants)).value = value
 REDIM PRESERVE constants(UBOUND(constants) + 1)
END SUB
