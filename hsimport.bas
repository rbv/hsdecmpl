' HSIMPORT, (C) Ralph Versteegen 2004
'
' Released into the Public Domain
' (There is absolutely no reason to reuse any of this horrible code though)
'
' This is QuickBasic source.

DECLARE SUB copyhs ()
DECLARE FUNCTION ProgramName$ ()
DEFINT A-Z

CONST FALSE = 0
CONST TRUE = -1

'PDS users should change the next line to include the QBX.BI file
'$INCLUDE: 'QBX.BI'

RANDOMIZE TIMER
progname$ = ProgramName$
starpointer% = 1
WHILE INSTR(starpointer% + 1, progname$, "\")
 starpointer% = INSTR(starpointer% + 1, progname$, "\")
WEND
pathfile$ = MID$(progname$, 1, starpointer%)
rpgfile$ = MID$(progname$, starpointer% + 1)
rpgfile$ = MID$(rpgfile$, 1, INSTR(rpgfile$, ".") - 1)
PRINT "prog path = "; pathfile$
PRINT "prog name = "; rpgfile$
namet$ = COMMAND$
IF LCASE$(rpgfile$) = "hsimport" OR namet$ = "" THEN
 PRINT "Usage:"
 PRINT "Rename hsimport.exe to the name of your RPG, eg sample.rpg"
 PRINT "This way, the program will know which RPG file to import"
 PRINT "the HS file into. Then drag and drop the desired HS file"
 PRINT "onto the executable to import."
 WHILE INKEY$ = "": WEND
 END
END IF
'INPUT "Input file"; infile$
infile$ = pathfile$ + rpgfile$ + ".rpg"
OPEN infile$ FOR BINARY AS 1
flen& = LOF(1)
IF flen& < 2 THEN
 PRINT "Error- file does not exist"
 CLOSE
 KILL infile$
 WHILE INKEY$ = "": WEND
 END
END IF
'task% = 0
inchar$ = " "
name$ = ""
rndno% = RND * 1000
randfile$ = pathfile$ + "hsimpbak.bak"
tempfile$ = pathfile$ + "tempfile.imp"
PRINT tempfile$
OPEN tempfile$ FOR BINARY AS 2
IF LOF(2) > 1 THEN
 PRINT LOF(2)
 PRINT "Tempfile, "; tempfile$; " already exists. Hsimport crashed last time."
 CLOSE
 WHILE INKEY$ = "": WEND
 END
END IF
PRINT namet$
OPEN namet$ FOR BINARY AS 3
blank$ = SPACE$(40)

WHILE SEEK(1) < flen&
 name$ = ""
 GET 1, , inchar$
 WHILE ASC(inchar$) <> 0
	name$ = name$ + inchar$
	IF LEN(name$) > 12 THEN
	 PRINT "Error: Incorrectly formatted RPG file: lumped file name too long"
	 CLOSE
	 WHILE INKEY$ = "": WEND
	 END -1
	END IF
	GET 1, , inchar$
 WEND
 LOCATE 5, 1
 PRINT blank$
 PRINT blank$
 PRINT blank$
 LOCATE 5, 1
 PRINT name$
 IF LCASE$(name$) = LCASE$(rpgfile$) + ".hsp" THEN
	t1$ = " "
	t2$ = " "
	GET 1, , temp1%
	GET 1, , t1$
	GET 1, , t2$
 ' PRINT temp1%; ASC(t1$); ASC(t2$)
	antioverflow& = ASC(t2$)  'stupid QB crashs in case  x& = y% * 600 if y% * 600 > 32k
	datalen& = ASC(t1$) + antioverflow& * 256 + 65536 * temp1% 'wait here! what negative?
	PRINT datalen&; "bytes"
	PRINT "Copying HS file"
	SEEK 1, (SEEK(1) + datalen&)
	outstr$ = name$ + CHR$(0)
	PUT 2, , outstr$
	outlen% = LOF(3)
	nil% = 0
	PUT 2, , outlen%
	PUT 2, , nil%
	datalen& = LOF(3)
	bigdat$ = SPACE$(50)
	WHILE SEEK(3) <= datalen&
	 IF SEEK(3) <= datalen& - 49 THEN
		GET 3, , bigdat$
		PUT 2, , bigdat$
	 ELSE
		GET 3, , dat$
		PUT 2, , dat$
	 END IF
	WEND
 ELSE
	t1$ = " "
	t2$ = " "
	name$ = name$ + CHR$(0)
	PUT 2, , name$
	GET 1, , temp1%
	GET 1, , t1$
	GET 1, , t2$
	PUT 2, , temp1%
	PUT 2, , t1$
	PUT 2, , t2$
 ' PRINT temp1%; ASC(t1$); ASC(t2$)
	antioverflow& = ASC(t2$)  'stupid QB crashs in case  x& = y% * 600 if y% * 600 > 32k
	datalen& = ASC(t1$) + antioverflow& * 256 + 65536 * temp1% 'wait here! what negative?
	PRINT datalen&; "bytes"
	PRINT "position 1="; SEEK(1); " position 2="; SEEK(2)
	dat$ = " "
	bigdat$ = SPACE$(400)
	startp& = SEEK(2)
	WHILE SEEK(2) < startp& + datalen&
	 IF SEEK(2) < startp& + datalen& - 399 THEN
		GET 1, , bigdat$
		PUT 2, , bigdat$
	 ELSE
		GET 1, , dat$
		PUT 2, , dat$
	 END IF
	WEND
 END IF
WEND
CLOSE
OPEN randfile$ FOR BINARY AS 99
CLOSE
KILL randfile$
NAME infile$ AS randfile$
NAME tempfile$ AS infile$
SYSTEM

FUNCTION ProgramName$

DIM Regs AS RegType

'Get PSP address

Regs.ax = &H6200
CALL Interrupt(&H21, Regs, Regs)
PSPSegment = Regs.bx

'Find environment address from PSP

DEF SEG = PSPSegment
EnvSegment = PEEK(&H2D) * 256 + PEEK(&H2C)

'Find the filename

DEF SEG = EnvSegment
EOT = FALSE 'Set end of environment table flag
Offset = 0

WHILE NOT EOT
Byte = PEEK(Offset) 'Get table character
IF Byte = 0 THEN 'End of environment string?
' PRINT 'Uncomment to print environment
Offset = Offset + 1
Byte = PEEK(Offset)
IF Byte = 0 THEN 'End of environment?
Offset = Offset + 3 'Yes - Skip over nulls & tbl info
C% = PEEK(Offset)
WHILE C% <> 0 'Assemble filename string
FileN$ = FileN$ + CHR$(C%) ' from individual
Offset = Offset + 1 ' characters
C% = PEEK(Offset)
WEND
EOT = TRUE 'Set flag to exit while/wend loop
END IF
ELSE 'No-Read more environment string
' PRINT CHR$(Byte); 'Uncomment to print environment
Offset = Offset + 1
END IF
WEND
ProgramName$ = FileN$
DEF SEG
END FUNCTION

